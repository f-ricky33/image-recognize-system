const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const session = require("express-session");
const validUrl = require("valid-url");
const zipper = require("zip-local");
const GetImageHelper = require("./get_images_helper");
const ScrapperPlugin = require("./scraper_plugin");
const globalFunctions = require("./functions");
const port = 3005;
const app = express();
const { exec } = require("child_process");
fs = require("fs");
let TOKENS = [];
const isImage = require("is-image");
var nudity = require("./nudity/nudity");
var shell = require("shelljs");

//sudo kill -9 $(sudo lsof -t -i:3005)

var UserModel = require("./models/User");
var database = require("./services/db.js");
const { resourceUsage } = require("process");
global.userObject = null;

const publicDir = "/public";
const publicVkDir = "/vk/results";
const publicTgDir = "/telegram/results";
const archiveDir = "/archives";
app.use(express.static(__dirname + publicDir));
app.use(express.static(__dirname + publicVkDir));
app.use(express.static(__dirname + publicTgDir));

app.use(bodyParser.json());
const FULL_PROGRESS = 100;
let allSites = [];
const destinationDirectory = path.join(__dirname, "work/result");

app.use(
  session({
    secret: "secret123",
    saveUninitialized: true,
  })
);

/*
  Глобальный объект параметров запуска
*/
global.startParameters = {
  depthLevel: 1,
  useDelayRange: 0,
  delayStart: 0,
  delayEnd: 0,
  accuracy: 0,
};

/*
  Глобальный объект текущего сайта
*/
global.siteInfo = {
  homeUrl: "",
  allUrls: [],
  sourceDir: "",
  resultDir: "",
  currentRandomName: "",
};

/*
  Глобальный объект текущего прогресса
*/
global.progressResult = {
  progress: 0,
  errorSite: [],
  logMessage: "",
  resultArhciveLink: null,
};

class SystemAPI {
  constructor() {
    this.fullArchiveDir = path.join(
      __dirname,
      path.join(publicDir, archiveDir)
    );
    console.log(this.fullArchiveDir);
    // Запуск сервера на порту {port}
    const server = app.listen(port, (error) => {
      if (error) return console.log(`Error: ${error}`);

      console.log(`Server listening on port ${server.address().port}`);

      this.runSearchImagesOnSitesAPI();

      this.runProgressAPI();

      this.runLoginAPI();

      this.runLogoutAPI();

      this.runRegisterAPI();

      this.runClearLogAPI();

      this.runProfileApi();

      this.runTelegramLoginApi();

      this.runTelegramSeacrhApi();

      this.runVkLoginApi();

      this.runVkSeacrhApi();
    });
  }

  /*
    Метод запускает основной эндпоинт для поиска картинок по сайтам
  */
  runSearchImagesOnSitesAPI() {
    app.post("/", async (req, res) => {
      console.log(req.body);
      this.clearSystemParams();

      if (!this.checkStartSeachParameters(req.body)) {
        progressResult.progress = 100;
        return;
      }

      let user_id = parseInt(req.body.user_id);

      // Получаем массив всех сайтов
      allSites = req.body.sourceList.split("\n");
      //Получаем параметры
      startParameters.depthLevel = req.body.depthLevel;

      // Задержка
      startParameters.useDelayRange = req.body.useDelayRange;
      if (startParameters.useDelayRange > 0) {
        startParameters.delayStart = req.body.delayStart;
        startParameters.delayEnd = req.body.delayEnd;
      }

      // Точность
      startParameters.accuracy = req.body.accuracy;

      try {
        let systemResult = await this.systemWorkStart(user_id);

        if (!systemResult) {
          return;
        }

        let archiveName = globalFunctions.generateRandomName(
          globalFunctions.randomInteger(7, 10)
        );

        if (!fs.existsSync(siteInfo.resultDir)) {
          fs.mkdir(siteInfo.resultDir, { recursive: true }, (err) => {
            if (err) {
              response.sendStatus(409);
              return;
            }

            archiveName += ".zip";
            console.log("Закончили");
            zipper.sync
              .zip(siteInfo.resultDir)
              .compress()
              .save(path.join(this.fullArchiveDir, archiveName));

            progressResult.resultArhciveLink = path.join(
              archiveDir,
              archiveName
            );

            progressResult.progress = parseInt(FULL_PROGRESS / allSites.length);
          });
        } else {
          archiveName += ".zip";
          console.log("Закончили");
          zipper.sync
            .zip(siteInfo.resultDir)
            .compress()
            .save(path.join(this.fullArchiveDir, archiveName));

          progressResult.resultArhciveLink = path.join(archiveDir, archiveName);

          progressResult.progress = parseInt(FULL_PROGRESS / allSites.length);
        }
      } catch (e) {
        globalFunctions.logMessage(
          `При работе системы возникла ошибка. Пожалуйста, попробуйте позже.`
        );
        console.log(e);
      }
    });
  }

  async systemWorkStart(user_id) {
    return new Promise((resolve, reject) => {
      try {
        allSites.forEach(async (el) => {
          // Проверяем на URL
          if (!validUrl.isUri(el)) {
            globalFunctions.logMessage(`Неверно указан адрес сайта: ${el}`);
            reject(false);
            return;
          }

          let imageHelper = new GetImageHelper(el, user_id);
          globalFunctions.logMessage(`Начало работы системы для сайта: ${el}`);

          await imageHelper.start();
          globalFunctions.logMessage(`Для сайта: ${el} закончили работу!`);

          resolve(true);
        });
      } catch (e) {
        console.log(e);
        reject(false);
      }
    });
  }

  /*
    Метод запускает эндпоинт для возврата прогресса
  */
  runProgressAPI() {
    app.get("/progress", (request, response) => {
      response.send(JSON.stringify(progressResult));
    });
  }

  /*
    Метод запускает эндпоинт для авторизации на сайте
  */
  runLoginAPI() {
    app.post("/login", async (request, response) => {
      try {
        // xss, sql
        let username = request.body.username ? request.body.username : null;
        let password = request.body.password ? request.body.password : null;

        if (!username || !password) {
          response.sendStatus(400);
          return;
        }

        let result = await database.fetchAll(
          "*",
          "users",
          `username='${username}'`
        );

        if (!result.length) {
          response.sendStatus(401);
          return;
        }

        let user = new UserModel({ username: username, password: password });

        if (!user.validPassword(user.password, result[0].password)) {
          response.sendStatus(401);
          return;
        }

        user = user.toAuthJSON();
        user.id = result[0].id;
        request.session.userObject = userObject = user;

        TOKENS.push({ id: user.id, token: user.token });
        TOKENS = globalFunctions.uniqueArray(TOKENS);

        response.status(200).send(userObject);
      } catch (e) {
        console.log(e);
        response.sendStatus(401);
      }

      return;
    });
  }

  /*
    Метод запускает эндпоинт для деавторизации на сайте
  */
  runLogoutAPI() {
    app.get("/logout", (request, response) => {
      try {
        userObject = request.session.userObject = null;
        let phone_number = request.query.phone
          ? request.query.phone.trim()
          : null;

        console.log(phone_number);

        if (phone_number != null) {
          phone_number = "+" + phone_number;
          let sessionFileTg = path.join(
            __dirname,
            `telegram/${phone_number}.session`
          );

          if (fs.existsSync(sessionFileTg)) {
            fs.unlinkSync(sessionFileTg);
          }

          let sessionFileVk = path.join(__dirname, `vk/vk_config.v2.json`);

          if (fs.existsSync(sessionFileVk)) {
            fs.unlinkSync(sessionFileVk);
          }
        }

        response.sendStatus(200);
      } catch (error) {
        console.log(error);
        response.send(400);
      }
    });
  }

  /*
    Метод запускает эндпоинт для регистрации на сайте
  */
  runRegisterAPI() {
    app.post("/register", async (request, response) => {
      try {
        let username = request.body.username ? request.body.username : null;
        let password = request.body.password ? request.body.password : null;

        if (!username || !password) {
          response.sendStatus(400);
        }

        let result = await database.fetchAll(
          "id",
          "users",
          `username='${username}'`
        );

        if (result.length) {
          response.status(409).send({ message: "Логин уже используется" });
          return;
        }

        let user = new UserModel({ username: username, password: password });
        user.setPassword(user.password);
        result = await database.insert("users", {
          username: user.username,
          password: user.password,
        });

        response.sendStatus(200);
      } catch (e) {
        console.log(e);
        response.sendStatus(400);
      }
      return;
    });
  }

  /*
    Метод запускает эндпоинт для очистки лога сообщений
  */
  runClearLogAPI() {
    app.get("/clearlog", (request, response) => {
      progressResult.logMessage = "";
      globalFunctions.logMessage("Лог успешно очищен!");

      response.send(JSON.stringify("success"));
    });
  }

  /*
    Метод возвращает последние 10 сохраненных проверок текущего пользовтателя
  */
  runProfileApi() {
    app.get("/profile", async (request, response) => {
      try {
        let user_id = request.query.user_id;
        let user_token = request.query.token;

        console.log(user_id);
        console.log(user_token);

        console.log(globalFunctions.getTokenById(TOKENS, user_id));

        // ПРОБЛЕМА С ТОКЕНОМ
        if (user_token != globalFunctions.getTokenById(TOKENS, user_id)) {
          let result = await database.fetchAll(
            "*",
            "links",
            "user_id=" + user_id
          );

          let resAnswer = [];
          for (let i = 0; i < result.length; i++) {
            resAnswer.push({
              name: result[i].linkname,
              link: result[i].link,
            });
          }

          response.status(200).send(resAnswer);
          return;
        }

        // response.sendStatus(403);
      } catch (e) {
        console.log(e);
        response.sendStatus(400);
      }
    });
  }

  /**
   * Метод для запуска точки для авторизации в Telegram
   */
  runTelegramLoginApi() {
    app.post("/login-telegram", async (request, response) => {
      let phone_number = request.body.phone;
      let code_number = request.body.code;

      if (
        !request.body.hasOwnProperty("phone") &&
        !request.body.hasOwnProperty("code")
      ) {
        response.sendStatus(409);
        return;
      }

      if (
        request.body.hasOwnProperty("phone") &&
        (!phone_number || phone_number.length == 0)
      ) {
        response.sendStatus(409);
        return;
      }

      if (
        request.body.hasOwnProperty("code") &&
        (!code_number || code_number.length == 0)
      ) {
        response.sendStatus(409);
        return;
      }

      if (!code_number || code_number.length == 0) {
        exec(
          `cd ${path.join(__dirname, "telegram")}; python3 auth.py ` +
            phone_number,
          (err, stdout, stderr) => {
            if (err) {
              console.log(err);
              response.sendStatus(409);
              return;
            }

            response.sendStatus(200);
          }
        );
      } else if (!phone_number || phone_number.length == 0) {
        fs.writeFile(
          path.join(__dirname, "telegram") + "/code.txt",
          code_number,
          function (err, data) {
            if (err) {
              response.sendStatus(409);
              return;
            }

            response.sendStatus(200);
          }
        );
      } else {
        response.sendStatus(409);
        return;
      }
    });
  }

  /**
   * Метод для запуска точки для поиска в телеграм каналах
   */
  runTelegramSeacrhApi() {
    app.post("/telegram", async (request, response) => {
      let channels = request.body.channels;
      let phone_number = request.body.phone;
      let limit = request.body.limit;
      let user_id = parseInt(request.body.user_id);
      let accuracy = request.body.accuracy;

      console.log(phone_number);

      if (isNaN(user_id) || user_id <= 0) {
        response.sendStatus(403);
        return;
      }

      if (channels.length == 0) {
        response.sendStatus(409);
        return;
      }

      if (!phone_number) {
        response.sendStatus(403);
        return;
      }

      limit = parseInt(limit);

      accuracy = accuracy ? parseInt(accuracy) : 0;

      // Получаем массив всех каналов
      channels = request.body.channels.split("\n");

      globalFunctions.logMessage(
        `Всего каналов для проверки:  ${channels.length}`
      );

      try {
        if (
          !fs.existsSync(path.join(__dirname, `telegram/sources/${user_id}`))
        ) {
          this.removeDir(path.join(__dirname, `telegram/sources/${user_id}`));
        }

        await this.getAndRecognizeContentsTg(
          channels,
          phone_number,
          limit,
          user_id,
          accuracy
        );

        globalFunctions.logMessage(`Система закончила работу!`);
        progressResult.progress = 100;

        // Заархивировать папку telegramResultDir ссылку положить в базу и отправить пользователю
        let archiveSourceDir = path.join(
          __dirname,
          `telegram/results/${user_id}`
        );

        let archiveResultDir = path.join(
          __dirname,
          `telegram/results/archives/${user_id}`
        );

        if (fs.existsSync(archiveSourceDir)) {
          if (!fs.existsSync(archiveResultDir)) {
            await fs.mkdir(archiveResultDir, { recursive: true }, (err) => {
              if (err) {
                response.sendStatus(409);
                return;
              }
            });
          }

          let archiveName = globalFunctions.generateRandomName(
            globalFunctions.randomInteger(7, 10)
          );
          archiveName += ".zip";

          zipper.sync
            .zip(archiveSourceDir)
            .compress()
            .save(path.join(archiveResultDir, archiveName));

          this.removeDir(archiveSourceDir);
          this.removeDir(path.join(__dirname, `telegram/sources/${user_id}`));

          globalFunctions.saveLink(
            path.join(`archives/${user_id}`, archiveName),
            user_id,
            "tg(" + channels[0] + ")"
          );
        }

        response.sendStatus(200);
      } catch (error) {
        console.log(error);
        response.sendStatus(409);
      }
    });
  }

  removeDir(path2Dir) {
    return new Promise((resolve, reject) => {
      try {
        fs.rmdir(
          path2Dir,
          {
            recursive: true,
          },
          (error) => {
            if (error) {
              reject(false);
            }
  
            resolve(true);
          }
        );
      } catch (e) {
        console.log(e);
        reject(false);
      }
     
    });
  }

  /**
   * Метод для запуска точки для авторизации в vk
   */
  runVkLoginApi() {
    app.post("/login-vk", async (request, response) => {
      let phone_number = request.body.phone;
      let password = request.body.password;

      if (!phone_number || phone_number.length == 0) {
        response.sendStatus(409);
        return;
      }

      if (!password || password.length == 0) {
        response.sendStatus(409);
        return;
      }

      exec(
        `cd ${path.join(__dirname, "vk")}; python3 auth.py ` +
          phone_number +
          " " +
          password,
        (err, stdout, stderr) => {
          if (err) {
            console.log(err);
            response.sendStatus(403);
            return;
          }

          if (
            stdout.toLocaleLowerCase().trim() ==
            "Bad password".toLocaleLowerCase()
          ) {
            response.sendStatus(403);
            return;
          }

          response.sendStatus(200);
        }
      );
    });
  }

  /**
   * Метод для запуска точки для поиска вконтакте
   */
  runVkSeacrhApi() {
    app.post("/vk", async (request, response) => {
      let channels = request.body.channels;
      let phone_number = request.body.phone;
      let limit = request.body.limit;
      let user_id = parseInt(request.body.user_id);
      let accuracy = request.body.accuracy;

      console.log(phone_number);

      if (isNaN(user_id) || user_id <= 0) {
        response.sendStatus(403);
        return;
      }

      if (channels.length == 0) {
        response.sendStatus(409);
        return;
      }

      if (!phone_number) {
        response.sendStatus(403);
        return;
      }

      limit = limit ? parseInt(limit) : 10;

      accuracy = accuracy ? parseInt(accuracy) : 0;

      // Получаем массив всех каналов
      channels = request.body.channels.split("\n");

      globalFunctions.logMessage(
        `Всего источников для проверки:  ${channels.length}`
      );

      try {
        if (!fs.existsSync(path.join(__dirname, `vk/sources/${user_id}`))) {
          this.removeDir(path.join(__dirname, `vk/sources/${user_id}`));
        }

        await this.getAndRecognizeContentsVk(
          channels,
          phone_number,
          limit,
          user_id,
          accuracy
        );

        globalFunctions.logMessage(`Система закончила работу!`);
        progressResult.progress = 100;

        // Заархивировать папку telegramResultDir ссылку положить в базу и отправить пользователю
        let archiveSourceDir = path.join(__dirname, `vk/results/${user_id}`);

        let archiveResultDir = path.join(
          __dirname,
          `vk/results/archives/${user_id}`
        );

        if (fs.existsSync(archiveSourceDir)) {
          if (!fs.existsSync(archiveResultDir)) {
            await fs.mkdir(archiveResultDir, { recursive: true }, (err) => {
              if (err) {
                response.sendStatus(409);
                return;
              }
            });
          }

          let archiveName = globalFunctions.generateRandomName(
            globalFunctions.randomInteger(7, 10)
          );
          archiveName += ".zip";

          zipper.sync
            .zip(archiveSourceDir)
            .compress()
            .save(path.join(archiveResultDir, archiveName));

          this.removeDir(archiveSourceDir);
          this.removeDir(path.join(__dirname, `vk/sources/${user_id}`));

          globalFunctions.saveLink(
            path.join(`archives/${user_id}`, archiveName),
            user_id,
            "vk(" + channels[0] + ")"
          );
        }

        response.sendStatus(200);

        // progressResult.progress = parseInt(FULL_PROGRESS / allSites.length);
      } catch (error) {
        console.log(error);
        response.sendStatus(409);
      }
    });
  }

  getAndRecognizeContentsTg(channels, phone_number, limit, user_id, accuracy) {
    return new Promise(async (resolve, reject) => {
      try {
        let i = 0;
        let j = 0;
        for (const channel of channels) {
          let telegramResultDir = path.join(
            __dirname,
            `telegram/results/${user_id}/${channel}`
          );

          globalFunctions.logMessage(`Начинаем поиск для канала ${channel}`);
          await this.startCheckChannel(channel, phone_number, limit, user_id);
          i++;

          let work_dir = path.join(
            __dirname,
            `telegram/sources/${user_id}/${channel}`
          );
          let channelImages = await this.readDirtoArray(work_dir);

          console.log(channelImages.length);

          for (const image of channelImages) {
            if (isImage(path.join(work_dir, image))) {
              await ScrapperPlugin.runThreadForDetectImage(
                path.join(work_dir, image),
                telegramResultDir,
                ScrapperPlugin.finishRecognizeCallback
              );

              if (fs.existsSync(path.join(work_dir, image))) {
                let nudityPredictions = await nudity.nudityDetect(
                  path.join(work_dir, image)
                );

                console.log(nudityPredictions);
                nudityPredictions.forEach(async (el) => {
                  if (
                    (el.className == "Porn" && el.probability > 0.8) ||
                    (el.className == "Hentai" && el.probability > 0.8) ||
                    (el.className == "Sexy" && el.probability > 0.8)
                  ) {
                    if (fs.existsSync(path.join(work_dir, image))) {
                      await this.moveFile(
                        path.join(work_dir, image),
                        telegramResultDir
                      );
                    }
                  }
                });
              }
            }
            j++;
            await this.sleep(100);
            progressResult.progress = Math.round(
              (j / channels.length) * channelImages.length
            );
          }

          globalFunctions.logMessage(
            `Проверено каналов ${i + 1} из ${channels.length}`
          );
        }
        resolve(true);
      } catch (e) {
        console.log("222222222222222222222222");
        console.log(e);
        reject(false);
      }
    });
  }

  readDirtoArray(dir) {
    return new Promise((resolve, reject) => {
      fs.readdir(dir, function (err, files) {
        if (err) {
          resolve(null);
        }

        resolve(files);
      });
    });
  }

  getAndRecognizeContentsVk(channels, phone_number, limit, user_id, accuracy) {
    return new Promise(async (resolve, reject) => {
      try {
        let i = 0;
        let j = 0;
        for (const channel of channels) {
          let vkResultDir = path.join(
            __dirname,
            `vk/results/${user_id}/${channel}`
          );

          globalFunctions.logMessage(`Начинаем поиск для источника ${channel}`);
          let offset = 0;
          console.log(limit);
          if (limit <= 100) {
            await this.startCheckChannelVk(
              channel,
              phone_number,
              limit,
              user_id,
              offset
            );
          } else {
            let countIter = Math.round(limit / 100);
            let indexes = Array.apply(null, { length: countIter }).map(
              Number.call,
              Number
            );

            for (const k of indexes) {
              offset = k * 100;
              limit = 100;
              console.log(limit);
              console.log(offset);
              await this.startCheckChannelVk(
                channel,
                phone_number,
                limit,
                user_id,
                offset
              );
            }
          }

          i++;

          let work_dir = path.join(
            __dirname,
            `vk/sources/${user_id}/${channel}`
          );

          let channelImages = await this.readDirtoArray(work_dir);

          console.log(channelImages.length);

          for (const image of channelImages) {
            if (isImage(path.join(work_dir, image))) {
              await ScrapperPlugin.runThreadForDetectImage(
                path.join(work_dir, image),
                vkResultDir,
                ScrapperPlugin.finishRecognizeCallback
              );

              if (fs.existsSync(path.join(work_dir, image))) {
                let nudityPredictions = await nudity.nudityDetect(
                  path.join(work_dir, image)
                );

                console.log(nudityPredictions);
                nudityPredictions.forEach(async (el) => {
                  if (
                    (el.className == "Porn" && el.probability > 0.8) ||
                    (el.className == "Hentai" && el.probability > 0.8) ||
                    (el.className == "Sexy" && el.probability > 0.8)
                  ) {
                    if (fs.existsSync(path.join(work_dir, image))) {
                      await this.moveFile(
                        path.join(work_dir, image),
                        vkResultDir
                      );
                    }
                  }
                });
              }
            }
            j++;
            await this.sleep(100);
            progressResult.progress = Math.round(
              (j / channels.length) * channelImages.length
            );
          }

          globalFunctions.logMessage(
            `Проверено источников ${i + 1} из ${channels.length}`
          );
        }
        resolve(true);
      } catch (e) {
        console.log(e);
        reject(false);
      }
    });
  }

  startCheckChannel(channel, phone_number, limit, user_id) {
    return new Promise((resolve, reject) => {
      exec(
        `cd ${path.join(
          __dirname,
          "telegram"
        )}; python3 get_channel_images.py ${phone_number} ${channel}  ${limit} ${user_id}`,
        (err, stdout, stderr) => {
          if (err) {
            console.log(err);
            reject(false);
          }

          resolve(true);
        }
      );
    });
  }

  startCheckChannelVk(channel, phone_number, limit, user_id, offset) {
    return new Promise((resolve, reject) => {
      exec(
        `cd ${path.join(
          __dirname,
          "vk"
        )}; python3 vk.py ${phone_number} ${channel}  ${limit} ${user_id} ${offset}`,
        (err, stdout, stderr) => {
          if (err) {
            console.log(err);
            reject(false);
          }

          console.log(stdout);
          if (stdout.toLocaleLowerCase().trim() == "403".toLocaleLowerCase()) {
            reject(false);
            return;
          }

          resolve(true);
        }
      );
    });
  }

  /*
    Метод сбрасывает системные параметры к исходным значениям
    для повторного запуска
  */
  clearSystemParams() {
    siteInfo.homeUrl = "";
    siteInfo.allSites = [];
    siteInfo.sourceDir = "";
    siteInfo.resultDir = "";

    progressResult.progress = 0;
    progressResult.logMessage = "";
  }

  /**
    Метод для проверки передаваемых с фронтенда параметров.
    * @param {Object} parameters - объект параметров
  */
  checkStartSeachParameters(parameters) {
    if (parameters.sourceList == "") {
      globalFunctions.logMessage("Задан пустой список источников!</br>");
      return false;
    }

    if (parameters.depthLevel < 1 || parameters.depthLevel > 10) {
      globalFunctions.logMessage(
        "Неправильно задано значение глубины поиска! Требуется значение от 1 до 10 включительно."
      );
      return false;
    }

    if (parameters.useDelayRange < 0) {
      globalFunctions.logMessage("Неправильно задан параметр глубины поиска!");
      return false;
    }

    if (parameters.useDelayRange > 0) {
      if (parameters.delayStart > parameters.delayEnd) {
        globalFunctions.logMessage(
          "Диапазон значений параметра задержки задан неверно. Стартовое значение не может быть больше конечного."
        );
        return false;
      }

      if (
        parameters.delayStart < 0 ||
        parameters.delayStart > 2000 ||
        parameters.delayEnd < 0 ||
        parameters.delayEnd > 2000
      ) {
        globalFunctions.logMessage(
          "Диапазон значений для параметра задержки задан неверно."
        );
        return false;
      }
    }

    if (parameters.accuracy < 0 || parameters.accuracy > 100) {
      globalFunctions.logMessage("Неправильно задан параметр точности!</br>");
      return false;
    }

    return true;
  }

  sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }

  /*
    Метод для перемещения файла из одной директории в другую
*/
  async moveFile(source, dest) {
    return new Promise((resolve, reject) => {
      try {
        if (!fs.existsSync(dest)) {
          shell.mkdir("-p", dest);
        }

        if (fs.existsSync(source)) {
          let command = `mv "${source}" "${dest}"`;

          exec(command, (error, stdout, stderr) => {
            if (error) {
              console.log("3333333333");
              reject(error);
              return;
            }

            resolve(true);
          });
        }
      } catch (e) {
        console.log(e);
        reject(false);
      }
    });
  }
}

module.exports = SystemAPI;
