#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import vk_api
import urllib.request


def getImages(channel, phone, limit, user_id, offset):
    # ======= Открываем сессию  с VK =======
    vk_session = vk_api.VkApi(phone)
    try:
        vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return

    try:
        vk = vk_session.get_api()

        # ======= начинаем перебирать каждого пользователя =======
        # посылаем запрос к VK API, count свой
        posts = vk.wall.get(owner_id=int(channel), count=limit, offset=offset)

        newpath = os.path.join(os.path.join(sys.path[0] + '/sources', user_id), channel)

        # работаем с каждой полученной фотографией
        for post in posts["items"]:

            # берём ссылку на максимальный размер фотографии
            if('attachments' in post):
                for attach in post['attachments']:
                    if('photo' in attach):
                        if('sizes' in attach['photo']):
                            # создаем директорию с именем пользователя, если нет
                            if not os.path.exists(newpath):
                                os.makedirs(newpath)

                            photo_url = str(attach['photo']["sizes"][len(
                                attach['photo']["sizes"])-1]["url"])
                            urllib.request.urlretrieve(
                                photo_url, newpath + '/' + str(post['id']) + '.jpg')

        if len(os.listdir(newpath)) == 0:
            os.remove(newpath)
    except Exception as e:
          print(e)
          return

if __name__ == "__main__":
    phone = sys.argv[1]
    group = sys.argv[2]
    limit = sys.argv[3]
    user_id = sys.argv[4]
    offset = sys.argv[5]

    if not os.path.exists(os.path.join(sys.path[0], 'vk_config.v2.json')):
        print('403')
        quit()

    getImages(group, phone, limit, user_id, offset)
