#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import vk_api
import urllib.request

def authVk(phone, password):
    # ======= Открываем сессию  с VK =======
    vk_session = vk_api.VkApi(phone, password)
    try:
        vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return

if __name__ == "__main__":
    phone = sys.argv[1]
    password = sys.argv[2]
    authVk(phone, password)