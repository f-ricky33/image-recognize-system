# -*- coding: utf-8 -*-
from telethon import TelegramClient, sync
from telethon.tl.functions.messages import GetHistoryRequest
from time import sleep
import os
import sys

CONFING = {
    'api_id': 2706229,
    'api_hash': 'a8f2de14e20a2bd307502dd553eac71d',
}


def authTelegram(phone_number):
    client = TelegramClient(
        phone_number, CONFING['api_id'], CONFING['api_hash'])

    client.connect()

    return client


def checkChannel(client, channel, limit, user_id, offset):

    channel_entity = client.get_entity(channel)
    work_folder = os.getcwd() + '/sources/' + str(user_id)

    posts = client(GetHistoryRequest(
        peer=channel_entity,
        limit=limit,
        offset_date=None,
        offset_id=0,
        max_id=0,
        min_id=0,
        add_offset=offset,
        hash=0))

    if not os.path.exists(work_folder):
        os.mkdir(work_folder)

    work_folder += '/' + channel

    if not os.path.exists(work_folder):
        os.mkdir(work_folder)

    for el in posts.messages:
        if el.media is not None and el.photo:
            client.download_media(el.media, work_folder)

def __main__():

    phone_number = sys.argv[1]
    channel = sys.argv[2]
    limit = int(sys.argv[3])
    user_id = int(sys.argv[4])

    if(not os.path.exists(os.getcwd() + '/' + phone_number + '.session')):
        raise Exception("Вы не авторизованы")

    client = authTelegram(phone_number)

    if(limit > 1000):
        offset = 0
        while limit > 0:
            print(limit)
            newlimit = 1000 if limit > 1000 else limit
            checkChannel(client, channel, newlimit, user_id, offset)
            limit -= newlimit
            offset += newlimit
    else:
        checkChannel(client, channel, limit, user_id, 0)

    # client.download_media(posts.messages[3].media, os.chdir(os.getcwd()))

    return


__main__()
