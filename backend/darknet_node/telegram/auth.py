# -*- coding: utf-8 -*-
from telethon import TelegramClient, sync
from telethon.tl.functions.messages import GetHistoryRequest
from time import sleep
import os
import sys

CONFING = {
    'api_id': 2706229,
    'api_hash': 'a8f2de14e20a2bd307502dd553eac71d',
}

def getCodeNumber():

    code_file = os.getcwd() + '/code.txt'

    while(1):
        if(os.path.exists(code_file)):
            with open(code_file, "r") as fd:
                code = fd.read()
                if not code:
                    sleep(1)
                    continue
            os.remove(code_file)
            return code
        else:
            sleep(1)

# Метод проверяет наличие сессионного файла


def checkSession(phone_number):
    client = TelegramClient(
        phone_number, CONFING['api_id'], CONFING['api_hash'])
    client.start(phone=phone_number, code_callback=getCodeNumber)

    return client

# Метод для получения клиента телеграм


def clienConnect(phone_number):

    if(not os.path.exists(os.getcwd() + '/' + phone_number + '.session')):
        client = checkSession(phone_number)
    else:
        client = TelegramClient(
            phone_number, CONFING['api_id'], CONFING['api_hash'])
        client.connect()

    return client


def __main__():

    phone_number = sys.argv[1]

    if(os.path.exists(os.getcwd() + '/' + phone_number + '.session')):
        quit()

    # Подключаемся к телеграм
    client = clienConnect(phone_number)


__main__()