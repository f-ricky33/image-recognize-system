var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var secret = 'secret';

var UserSchema = new mongoose.Schema({
    username: {type: String, lowercase: true, required: [true, "can't be blank"], match: [/^[a-zA-Z0-9]+$/, 'is invalid'], index: true},
    password: {type: String},
}, {timestamps: true});


UserSchema.methods.setPassword = function(password){
    this.password = crypto.pbkdf2Sync(password, 'test', 10000, 256, 'sha256').toString('hex');
};

UserSchema.methods.validPassword = function(inputPassword, dbHash) {
    var hash = crypto.pbkdf2Sync(inputPassword, 'test', 10000, 256, 'sha256').toString('hex');
    return dbHash === hash;
};

UserSchema.methods.generateJWT = function() {
    var today = new Date();
    var exp = new Date(today);
    exp.setDate(today.getDate() + 60);
    return jwt.sign({
        id: this._id,
        username: this.username,
        exp: parseInt(exp.getTime() / 1000),
    }, secret);
};

UserSchema.methods.toAuthJSON = function(){
    return {
        username: this.username,
        token: this.generateJWT(),
    };
};

module.exports = User = mongoose.model('User', UserSchema);

// https://thinkster.io/tutorials/node-json-api/creating-the-user-model