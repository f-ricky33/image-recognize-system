var database = require("./services/db.js");

module.exports = {
  /**
   * Метод пишет одно сообщение в лог
   * @param {String} message
   */
  logMessage(message) {
    let dateStr = new Date().toLocaleTimeString("ru-RU", {
      year: "numeric",
      month: "numeric",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    });

    dateStr = dateStr.split(",").join(" -");
    progressResult.logMessage =
      "<b>[" +
      dateStr +
      "]: </b>" +
      message +
      "<br>" +
      progressResult.logMessage;
  },

  /** 
    Метод генерирует случайную строку заданной длины
    @param {String length} - длина
  */
  generateRandomName(length) {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  },

  /**
  Метод генерирует случайное число в промежутке от min до max
  @param {Int min}
  @param {Int max}
*/
  randomInteger(min, max) {
    // получить случайное число от (min-0.5) до (max+0.5)
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
  },

  /*
    Функция для сохранения ссылки в базе данных
  */
  async saveLink(link, userId, linkName) {
    try {

      let dateStr = new Date().toLocaleTimeString("ru-RU", {
        year: "numeric",
        month: "numeric",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
      });

      linkname = dateStr + '-' + linkName + '.zip',

      await database.insert("links", {
        link: link,
        user_id: userId,
        linkname: dateStr + '-' + linkName + '.zip',
      });
    } catch (e) {
      console.log("Ошибка при сохранении ссылки");
    }
  },

  /**
    Метод оставляет в массиве уникальные объекты
    @param  {array arr}
  */
  uniqueArray(arr) {
    let uniqueArray = [];

    for (let i = 0; i < arr.length; i++) {
      if (!this.elem_in_array(uniqueArray, arr[i])) {
        uniqueArray.push(arr[i]);
      }
    }

    return uniqueArray;
  },

  /**
   * Метод проверяет наличие item в массиве array
   * @param {Array} arr
   * @param {Object} item
   */
  elem_in_array(arr, item) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].id === item.id) {
        return true;
      }
    }

    return false;
  },
  /**
   * Метод возвращает токен по user_id
   * @param {Array} TOKENS
   * @param {int} user_id
   */
  getTokenById(TOKENS, user_id) {
    for (let i = 0; i < TOKENS.length; i++) {
      if (TOKENS[i].id === user_id) {
        return TOKENS[i].token;
      }
    }
    return null;
  },
};
