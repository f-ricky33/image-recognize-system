const path = require("path");
const GetSiteUrls = require("get-site-urls");
const scrape = require("website-scraper");
const ScrapeThrottler = require("./scrape_throttler");
const ScraperPlugin = require("./scraper_plugin");
const globalFunctions = require("./functions");
const clearDir = require("empty-folder");
const { exec } = require("child_process");

const sourceDirectory = path.join(__dirname, "work/source");
const destinationDirectory = path.join(__dirname, "work/result");

class GetImageHelper {
  // для обработки блоками

  constructor(site, user_id) {
    siteInfo.homeUrl = site;
    siteInfo.sourceDir = path.join(sourceDirectory, this.extractHostname(site));

    // Генерируем случайное имя для результирующей папки
    siteInfo.currentRandomName = globalFunctions.generateRandomName(
      globalFunctions.randomInteger(10, 15)
    );
    siteInfo.resultDir = path.join(
      path.join(destinationDirectory, user_id.toString()),
      this.extractHostname(site)
    );
  }

  /*
    Функция для первоначальной инициализации
    (удаление данных прошлого запуска)
  */
  async start() {
    return new Promise(async (resolve, reject) => {
      // Очищаем директории для работы
      try {
        let result = await this.clearDirectory(sourceDirectory);
        result = await this.clearDirectory(destinationDirectory);

        globalFunctions.logMessage("Рекурсивно ищем страницы сайта!");
        progressResult.progress = 10;

        siteInfo.allUrls = await this.getUrlsFromWebSite(
          startParameters.depthLevel
        );

       // await this.filterUrls();

        //console.log(siteInfo.allUrls.splice(25, siteInfo.allUrls.length-25));

        //console.log(siteInfo.allUrls);

        // // Старт поиска и распознавания изображений
        await this.siteImagesScraper();
        resolve(true);
      } catch (e) {
        console.log(e);
        reject(e);
      }
    });
  }

  /*
    Рекурсивно удалить все файлы и поддиректории
  */
  clearDirectory(pathToDir) {
    return new Promise((resolve, reject) => {
      clearDir(pathToDir, false, (o) => {
        if (o.error) reject(new Error(o.error));
        resolve(true);
      });
    });
  }

  /*
    Метод извлекает domain из url
  */
  extractHostname(url) {
    let hostname;

    if (url.indexOf("//") > -1) {
      hostname = url.split("/")[2];
    } else {
      hostname = url.split("/")[0];
    }

    hostname = hostname.split(":")[0];
    hostname = hostname.split("?")[0];

    return hostname;
  }

  /*
    Метод собирает ссылки на страницы сайта
  */
  async getUrlsFromWebSite(maxDepth) {
    return new Promise(async (resolve, reject) => {
      try {
        exec(
          `cd ${path.join(__dirname, "crawl_urls")}; python3 get_urls.py ${
            siteInfo.homeUrl
          } ${maxDepth}`,
          (err, stdout, stderr) => {
            if (err) {
              console.log(err);
              reject(false);
              return;
            }

            resolve(
              fs
                .readFileSync(
                  path.join(__dirname, "crawl_urls/site-urls.txt"),
                  "utf8"
                )
                .toString()
                .split("\n")
            );
          }
        );

        // if (!links) {
        //   globalFunctions.logMessage(
        //     "При получении ссылок с сайта возникла ошибка! Пожалуйста, попробуйте позже."
        //   );
        //   reject(false);
        //   return;
        // }

        // globalFunctions.logMessage(
        //   `Обнаружено страниц: ${links.pages.length}.`
        // );
        // globalFunctions.logMessage(
        //   `Начинаем процесс получения изображений с сайта.`
        // );
        // resolve(links.pages);
      } catch (e) {
        globalFunctions.logMessage(
          "При получении ссылок с сайта возникла ошибка! Пожалуйста, попробуйте позже."
        );
        console.log(e);
        reject(false);
      }
    });
  }

  /*
    Метод загружает все изображения с сайта.
  */
  async siteImagesScraper() {
    return new Promise(async (resolve, reject) => {
      siteInfo.allUrls.splice(50, siteInfo.allUrls.length-50);

      
      siteInfo.allUrls = siteInfo.allUrls.filter((url) => {
        if (
          url.indexOf(" ") < 0 &&
          !(/[а-яА-ЯЁё]/.test(url)) && 
          url.indexOf(this.extractHostname(siteInfo.homeUrl)) >= 0
        ) {
          return url;
        }
      });

      console.log(siteInfo.allUrls);

      let options = {
        urls: siteInfo.allUrls,
        directory: siteInfo.sourceDir,
        sources: [{ selector: "img", attr: "src" }],
        plugins: [
          new ScraperPlugin(),
          new ScrapeThrottler(
            startParameters.delayStart,
            startParameters.delayEnd
          ),
        ],
      };

      scrape(options, (error, result) => {
        if (error) {
          progressResult.logMessage += globalFunctions.logMessage(
            "Возникла ошибка при загрузке изображений с сайта."
          );
          reject(new Error(error));
        }

        progressResult.logMessage += globalFunctions.logMessage(
          "Успешно загрузили все найденные изображения на сайте. Параллельно запущен процесс распознавания."
        );
        resolve(true);
      });
    });
  }
}

module.exports = GetImageHelper;
