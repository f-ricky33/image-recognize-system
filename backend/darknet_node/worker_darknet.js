const { workerData, parentPort } = require("worker_threads");
const Darknet = require("./darknet.js");
let allowedClasses;
const fs = require("fs");
const { exec } = require("child_process");
const path = require("path");
//console.log("path to image: " + workerData);

  /*
    Метод для перемещения файла из одной директории в другую
*/
async function moveFile(source, dest) {
  return new Promise((resolve, reject) => {
    if (!fs.existsSync(dest)) {
      shell.mkdir("-p", dest);
    }

    if (fs.existsSync(source)) {
      let command = `mv "${source}" "${dest}"`;
      
      exec(command, (error, stdout, stderr) => {
        if (error) {
          console.log('1111111111111111');
          reject(false);
          return;
        }

        resolve(true);
      });
    }
  });
}

async function selectAllowedImages(resultJson, pathToImage, destFolderPath) {
  return new Promise(async (resolve, reject) => {
    for (let i = 0; i < resultJson.length; i++) {
      if (
        allowedClasses
          .map((e) => e.toLowerCase())
          .includes(resultJson[i].class.toLowerCase())
      ) {
        if (!fs.existsSync(destFolderPath)) {
          fs.mkdir(destFolderPath, { recursive: true }, async (err) => {
            if (err) {
              reject(err);
              return;
            }

            await moveFile(pathToImage, destFolderPath);
            resolve(true);
            return;
          });
        } else {
          await moveFile(pathToImage, destFolderPath);
          resolve(true);
          return;
        }
      }
    }
    resolve(true);
  });
}

async function startDetectImage(pathToImage, destFolderPath) {
  try {
    let darknet = new Darknet();

    allowedClasses = fs
      .readFileSync(path.join(__dirname, "allowedClasses.txt"))
      .toString()
      .split("\r\n");

    let resultJson = await darknet.detect(pathToImage);

    await selectAllowedImages(resultJson, pathToImage, destFolderPath);

    parentPort.postMessage({ status: "Done" });
  } catch (e) {
    console.log(e);
    parentPort.postMessage({ status: "Error" });
  }
}

startDetectImage(workerData.imagePath, workerData.destDir);

