const jpeg = require("jpeg-js");
const tf = require("@tensorflow/tfjs-node");
const nsfw = require("nsfwjs");
const axios = require("axios");
var fs = require("fs");

let _model;

async function detectImage(imagePath) {
  return new Promise(async (resolve, reject) => {
    if (fs.existsSync(imagePath)) {
      fs.readFile(imagePath, async function (err, data) {
        if (err) {
          console.log(err);
          reject(false);
          return;
        }

        try {
          const image = await tf.node.decodeImage(data, 3);
          const predictions = await _model.classify(image);
          resolve(predictions);
        } catch (error) {
          console.log(error);
          reject(false);
        }
      });
    } else {
      resolve([]);
    }
  });
}

const load_model = async () => {
  _model = await nsfw.load();
};

module.exports = {
  async nudityDetect(imagePath) {
    return new Promise((resolve, reject) => {
      load_model().then(async () => {
        try {
          let result = await detectImage(imagePath);
          resolve(result);
          return;
        } catch (error) {
          console.log(error);
          reject(false);
        }
      });
    });
  },
};
