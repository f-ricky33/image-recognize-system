const mysql = require("mysql2");

let database = {
  con: null,
  createConnection: function () {
    this.con = this.con
      ? this.con
      : mysql.createConnection({
          host: "localhost",
          user: "root",
          password: "newpass",
          database: "diplom",
        });
  },
  fetchAll: function (fields, table, condition) {
    return new Promise((resolve, reject) => {
      if (!table) {
        throw err;
      }

      fields = fields ? fields : "*";
      condition = condition ? condition : "1=1";

      this.createConnection();

      this.con.connect((err) => {
        if (err) throw err;
        console.log("Connected!");
        var sql = `Select ${fields} from ${table} where ${condition}`;
        this.con.query(sql, function (err, result) {
          if (err) reject(err);
          resolve(result);
        });
      });
    });
  },
  insert: function (table, data) {
    if (!data) throw err;

    this.createConnection();

    let fields = "";
    let values = "";
    for (let key in data) {
      fields += `${key},`;
      values += `'${data[key]}',`;
    }

    if (fields == "" || values == "") {
      if (!data) throw err;
    }

    fields = fields.replace(/^,|,$/g, "");
    values = values.replace(/^,|,$/g, "");

    this.con.connect((err) => {
      if (err) throw err;
      console.log("Connected!");
      var sql = `INSERT INTO ${table} (${fields}) VALUES (${values})`;
      this.con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("1 record inserted");
      });
    });
  },
};

module.exports = database;
