const path = require("path");
const { Worker } = require("worker_threads");
const isImage = require("is-image");
const globalFunctions = require("./functions");

// kill -9 $(lsof -t -i:3005)
const delimValue = 5;

class ScrapperPlugin {
  static fullPath;

  static countRecognized = 0;
  static countDownloaded = 0;
  static lastCountDownloaded = 0;
  static threadObject = {
    countThread: 3,
    threadsFree: true,
    countThreadBusy: 0,
  };

  static recognizeObject = {
    siteImages: [],
    recognizedImages: [],
  };

  apply(registerAction) {
    registerAction("onResourceSaved", this.newFileHandler);

    registerAction("afterFinish", this.finishDownloading);
  }

  newFileHandler({ resource }) {
    console.log('Новый ресурс');
    if (isImage(resource.filename)) {
      ScrapperPlugin.fullPath = path.join(
        siteInfo.sourceDir,
        resource.filename
      );
      ScrapperPlugin.recognizeObject.siteImages.push(ScrapperPlugin.fullPath);
      ScrapperPlugin.countDownloaded++;
      if (ScrapperPlugin.countDownloaded % delimValue == 0) {
        ScrapperPlugin.lastCountDownloaded = ScrapperPlugin.countDownloaded;
        globalFunctions.logMessage(
          `Скачано изображений: <b>${ScrapperPlugin.countDownloaded}</b>`
        );
      }
    }
  }

  async finishDownloading() {
    try {
      if (
        ScrapperPlugin.lastCountDownloaded != ScrapperPlugin.countDownloaded
      ) {
        globalFunctions.logMessage(
          `Скачано изображений: <b>${ScrapperPlugin.countDownloaded}</b>`
        );
      }

      globalFunctions.logMessage(
        `Закончили поиск изображений для сайта ${siteInfo.homeUrl}`
      );
      globalFunctions.logMessage("Начинаем процесс распознавания!");

      for (
        let i = 0;
        i < ScrapperPlugin.recognizeObject.siteImages.length;
        i++
      ) {
        if (isImage(ScrapperPlugin.recognizeObject.siteImages[i])) {
          await ScrapperPlugin.runThreadForDetectImage(
            ScrapperPlugin.recognizeObject.siteImages[i],
            siteInfo.resultDir,
            ScrapperPlugin.finishRecognizeCallback
          );

          ScrapperPlugin.countRecognized++;

          console.log(ScrapperPlugin.countRecognized);
          console.log(ScrapperPlugin.countDownloaded);
          if (
            ScrapperPlugin.countRecognized % delimValue == 0 ||
            ScrapperPlugin.countRecognized >= ScrapperPlugin.countDownloaded
          ) {
            globalFunctions.logMessage(
              `Распознано изображений: <b>${ScrapperPlugin.countRecognized}</b>`
            );
          }
        }
      }

      globalFunctions.logMessage(`Система закончила поиск и распознавание`);
    } catch (e) {
      globalFunctions.logMessage(
        `При распознавании изображений возникла ошибка. Пожалуйста, попробуйте позже!`
      );
      console.log(e);
    }
  }

  /*
    Метод запускает распознвание изображения в новом потоке
  */
  static runThreadForDetectImage(imagePath, destDir, callbackFunc) {
    return new Promise((resolve, reject) => { 
      console.log(imagePath);
      const worker = new Worker(__dirname + "/worker_darknet.js", {
        workerData: { imagePath: imagePath, destDir: destDir, currentRandomName: siteInfo.currentRandomName},
      });
      worker.on("message", async () => {
        await callbackFunc(imagePath, true);
        resolve(true);
      });
      worker.on("error", async (e) => {
        console.log(e);
        await callbackFunc(imagePath, false);
        reject(false);
      });
      worker.on("exit", (code) => {
        if (code !== 0)
          reject(new Error(`В потоке распознавания возникла ошибка: ${code}`));
      });
    });
  }

  static async finishRecognizeCallback(imagePath, status) {
    return new Promise((resolve, reject) => {
      if (!status) return;

      console.log(imagePath + "- распознан");
      resolve(true);
      //console.log(ScrapperPlugin.recognizeObject.siteImages.length);
    });
  }
}

module.exports = ScrapperPlugin;
