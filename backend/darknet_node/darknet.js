const { exec } = require("child_process");

class Darknet {
  constructor(
    data = "cfg/imagenet1k.data",
    config = "cfg/darknet19.cfg",
    weights = "darknet19.weights"
  ) {
    this.data = data;
    this.config = config;
    this.weights = weights;
  }

  async detect(image) {
    return new Promise((resolve, reject) => {
      let command = `darknet classifier predict ${this.data} ${this.config} ${this.weights} "${image}"`;

      exec(command, (error, stdout, stderr) => {
        
        if(error){
          reject(new Error(error));
          return;
        }

        let jsonСlassProbs = this.resultToJson(stdout);
        resolve(jsonСlassProbs);
      });
    });
  }

  resultToJson(result) {
    let splitMass = result.split("\n");
    splitMass.pop();

    let resMassive = [];
    splitMass.forEach((el) => {
      el = el.replace("%", "").trim();
      let splitElement = el.split(": ");

      let elJson = {
        class: splitElement[1],
        prob: splitElement[0],
      };

      resMassive.push(elJson);
    });

    return resMassive;
  }
}

module.exports = Darknet;
