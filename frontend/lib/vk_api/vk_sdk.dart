// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;

class VkSdk {
  final String baseUrl = 'https://oauth.vk.com/authorize';
  final String clientId = "7606177";
  final String redirectUri = "http://localhost:18822/#/dashboard";
  final String scope = "docs";
  final String responseType = "code";
  final String apiVersion = "5.124";

  bool vkLogin() {
    String requestUrl = baseUrl +
        '?client_id=$clientId&response_type=token&scope=docs&redirect_uri=$redirectUri';

    print(requestUrl);
    html.window.open(requestUrl, 'Vk Login');

    return true;
  }
}
