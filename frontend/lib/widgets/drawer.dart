import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pentest_web_app/routes/router.dart';
import 'package:pentest_web_app/utils/constants.dart';

class DrawerMenu extends StatelessWidget {
  String login;
  int userId;

  DrawerMenu(this.login, this.userId);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(padding: EdgeInsets.all(0.0), children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: Text('Логин: ' + this.login),
          accountEmail: Text('id: ' + this.userId.toString()),
          currentAccountPicture: CircleAvatar(
            backgroundImage: ExactAssetImage('avatar.jpg'),
          ),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("fundo.jpg"), fit: BoxFit.cover)),
        ),
        ListTile(
          title: Text('Профиль'),
          leading: Icon(Icons.person),
          onTap: () {
            AppRouter().router().navigateTo(context, Constants.PROFILE_PAGE);
          },
        ),
        Divider(),
        ListTile(
          title: Text('Панель управления'),
          leading: Icon(Icons.admin_panel_settings),
          onTap: () {
            AppRouter().router().navigateTo(
                  context,
                  Constants.DASHBOARD_PAGE,
                );
          },
        ),
        Divider(),
        ListTile(
          title: Text('Помощь'),
          leading: Icon(Icons.report_problem),
        ),
        ListTile(
            title: Text('Закрыть'),
            leading: Icon(Icons.close),
            onTap: () {
              Navigator.of(context).pop();
            }),
      ]),
    );
  }
}
