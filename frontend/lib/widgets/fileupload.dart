import 'package:flutter/material.dart';
import 'dart:html' as html;
import 'dart:typed_data';
import 'dart:convert';
import 'package:pentest_web_app/pages/sites_page.dart';

class FileUploadApp extends StatefulWidget {
  @override
  createState() => _FileUploadAppState();
}

class _FileUploadAppState extends State<FileUploadApp> {
  List<int> _selectedFile;
  Uint8List _bytesData;
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  startWebFilePicker() async {
    html.InputElement uploadInput = html.FileUploadInputElement();
    uploadInput.accept = '.txt';
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      
      final file = uploadInput.files.first;
      final reader = new html.FileReader();

      if(file.type != 'text/plain'){
        print('Неверный формат файла!'); //Возможно, сообщение пользователю
        return;
      }

      reader.onLoadEnd.listen((e) {
        String base64Str = reader.result.toString().split(',')[1];
        String sites = utf8.decode(base64.decode(base64Str));

        SitesPage.getTextAreaField.text = sites;
      });
      reader.readAsDataUrl(file);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return SafeArea(
      child: Container(
        child: new Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ButtonTheme(
                    height: 50.0,
                    minWidth: 200.0,
                    child: RaisedButton(
                      elevation: 8.0,
                      hoverColor: Color.fromRGBO(49, 87, 110, 0.8),
                      color: Color.fromRGBO(49, 87, 110, 0.95),
                      textColor: Colors.white,
                      child: Text(
                        'Импорт из файла',
                        style: TextStyle(fontSize: 18.0),
                      ),
                      onPressed: () {
                        startWebFilePicker();
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
