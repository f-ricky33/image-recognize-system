class Progress {
  final double progress;
  final List<dynamic> errorSites;
  final String logMessage;
  final String resultArhciveLink;

  Progress({this.progress, this.errorSites, this.logMessage, this.resultArhciveLink});

  factory Progress.fromJson(Map<String, dynamic> json) {
    return Progress(
      progress: json['progress'] as double,
      errorSites: json['errorSite'] as List<dynamic>,
      logMessage: json['logMessage'] as String,
      resultArhciveLink: json['resultArhciveLink'] as String
    );
  }
}