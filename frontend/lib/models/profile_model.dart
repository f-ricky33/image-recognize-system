class Report {
  final String linkname;
  final String link;

  Report({this.linkname, this.link});

  factory Report.fromJson(Map<String, dynamic> json) {
    return Report(
      linkname: json['name'] as String,
      link: json['link'] as String,
    );
  }
}
