class DocRequest {
  Response response;

  DocRequest({this.response});

  DocRequest.fromJson(Map<String, dynamic> json) {
    response = json['response'] != null
        ? new Response.fromJson(json['response'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['response'] = this.response.toJson();
    }
    return data;
  }
}

class Response {
  int count;
  List<Items> items;

  Response({this.count, this.items});

  Response.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  int id;
  int ownerId;
  String title;
  int size;
  String ext;
  int date;
  int type;
  String url;
  String photo100;
  String photo130;

  Items(
      {this.id,
      this.ownerId,
      this.title,
      this.size,
      this.ext,
      this.date,
      this.type,
      this.url,
      this.photo100,
      this.photo130});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ownerId = json['owner_id'];
    title = json['title'];
    size = json['size'];
    ext = json['ext'];
    date = json['date'];
    type = json['type'];
    url = json['url'];
    photo100 = json['photo_100'];
    photo130 = json['photo_130'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['owner_id'] = this.ownerId;
    data['title'] = this.title;
    data['size'] = this.size;
    data['ext'] = this.ext;
    data['date'] = this.date;
    data['type'] = this.type;
    data['url'] = this.url;
    data['photo_100'] = this.photo100;
    data['photo_130'] = this.photo130;
    return data;
  }
}
