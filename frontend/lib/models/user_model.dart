class User {
  final int id;
  final String token;
  final String username;

  User({this.id, this.token, this.username});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'] as int,
      token: json['token'] as String,
      username: json['username'] as String,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'token': token,
        'username': username,
      };
}
