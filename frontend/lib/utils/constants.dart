class Constants {
  // роуты на фронтенде
  static const String HOME_PAGE = '/';
  static const String DASHBOARD_PAGE = '/dashboard';
  static const String SITE_SOURCES_PAGE = '/sources';
  static const String REGISTER_PAGE = '/register';
  static const String PROFILE_PAGE = '/profile';
  static const String TELEGRAM_PAGE = '/telegram';
  static const String VKONTAKTE_PAGE = '/vkontakte';

  // роуты на бекенде 
  static const String API_SERVER = 'http://localhost:3005/';
  static const String PROGRESS_ENDPOINT = 'progress';
  static const String CLEAR_LOG = 'clearlog';
  static const String PROFILE = 'profile';
  static const String LOGIN = 'login';
  static const String REGISTER = 'register';
  static const String LOGOUT = 'logout';
  static const String TELEGRAM_LOGIN = 'login-telegram';
  static const String TELEGRAM_SEARCH = 'telegram';
  static const String VKONTAKTE_LOGIN = 'login-vk';
  static const String VKONTAKTE_SEARCH = 'vk';

  static const String EMPTY_LOG_MESSAGE =
      'Поле для вывода сообщений от сервера!';
}

class ApplicationStatus {
  static const int NONE = 0;
  static const int START = 1;
  static const int PROCESSING = 2;
  static const int STOPPED = 3;
  static const int FINISH = 4;
}
