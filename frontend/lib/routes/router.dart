import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:pentest_web_app/pages/dashboard_page.dart';
import 'package:pentest_web_app/pages/login_page.dart';
import 'package:pentest_web_app/pages/profile_page.dart';
import 'package:pentest_web_app/pages/signup_page.dart';
import 'package:pentest_web_app/pages/sites_page.dart';
import 'package:pentest_web_app/pages/telegram_page.dart';
import 'package:pentest_web_app/pages/vkontakte.dart';
import '../pages/home_page.dart';

class AppRouter {
  static final AppRouter _instance = new AppRouter._internal();
  final FluroRouter _router = new FluroRouter(); // global router

  factory AppRouter() {
    return _instance;
  }
  AppRouter._internal();

  // singleton
  FluroRouter router() {
    return _router;
  }

  // static Fluro.Handler _homeHandler = Fluro.Handler(
  //     handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  //   return HomePage(params);
  // });

  static Handler _dashboardHandler =
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return DashBoardPage();
  });

  static Handler _homeHandler =
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return LoginScreen();
  });

  static Handler _welcomeHangler =
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return WelcomeScreen();
  });

  static Handler _registerHandler =
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return SignUpScreen();
  });

  static Handler _profileHanler =
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return ProfileScreen();
  });

  static Handler _sitesHandler =
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return SitesPage();
  });

  static Handler _telegramHandler =
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return TelegramPage();
  });

  static Handler _vkontakteHandler =
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return VkontaktePage();
  });

  void configureRoutes() {
    _router.define("/dashboard", handler: _dashboardHandler);
    //_router.define("/:vk", handler: _homeHandler);
    _router.define("/sources", handler: _sitesHandler);
    _router.define("/", handler: _homeHandler);
    _router.define("/welcome", handler: _welcomeHangler);
    _router.define("/register", handler: _registerHandler);
    _router.define("/profile", handler: _profileHanler);
    _router.define("/telegram", handler: _telegramHandler);
    _router.define("/vkontakte", handler: _vkontakteHandler);
  }
}
