import 'package:flutter/material.dart';
import 'package:pentest_web_app/routes/router.dart';
import 'package:pentest_web_app/utils/constants.dart';

main() {
  final appRouter = new AppRouter();
  appRouter.configureRoutes();
  return runApp(MyApp());
}

class MyApp extends StatefulWidget {
  static BuildContext appContext;

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    MyApp.appContext = context;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateRoute: AppRouter().router().generator,
      initialRoute: Constants.HOME_PAGE,
    );
  }
}
