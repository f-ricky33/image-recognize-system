import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pentest_web_app/models/progress_model.dart';
import 'package:pentest_web_app/models/user_model.dart';
import 'package:pentest_web_app/routes/router.dart';
import 'package:pentest_web_app/utils/constants.dart';
import 'package:pentest_web_app/widgets/drawer.dart';
import 'package:pentest_web_app/widgets/fileupload.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../function.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:http/http.dart' as http;
import 'dart:html' as html;
import 'package:pentest_web_app/pages/sites_page.dart';

String logMessage = Constants.EMPTY_LOG_MESSAGE;

class VkontaktePage extends StatefulWidget {
  @override
  _VkontakteState createState() => _VkontakteState();
}

class _VkontakteState extends State<VkontaktePage> {
  User user;
  int value = 0;
  int authState = 0; // Ввод номера
  int _buttonState = 0;
  String user_phone;
  int _appState = ApplicationStatus.NONE;

  int _useRequestHeader = 0;
  int _accuracyPercent = 0;
  int _progress = 0;
  bool _validateTextFieldEmpty = false;

  RangeValues _currentRangeDelayValues = const RangeValues(500, 1300);
  double _limitValue = 10;

  final GlobalKey<FormState> phoneFormKey = GlobalKey<FormState>();
  final GlobalKey<FormState> codeFormKey = GlobalKey<FormState>();
  final TextEditingController phone_controller = TextEditingController();
  TextEditingController code_controller = new TextEditingController();

  PhoneNumber phone_number = PhoneNumber(isoCode: 'RU');

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _getUser().then((value) => this.user = value);
      _getPhone().then((value) => this.user_phone = value);
    });
  }

  Future<String> _getPhone() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('phone_vk');
  }

  Future<User> _getUser() async {
    final prefs = await SharedPreferences.getInstance();
    return User.fromJson(json.decode(prefs.getString('user')));
  }

  @override
  Widget build(BuildContext context) {
    if (this.user == null) {
      Timer(Duration(milliseconds: 2000), () {
        setState(() {
          this.value = 1;
        });
      });

      return Scaffold(body: Center(child: CircularProgressIndicator()));
    }

    if (this.user_phone != null) {
      setState(() {
        this.authState = 2;
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Система поиска изображений".toUpperCase(),
          style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
        ),
        elevation: .1,
        backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 50.0),
            child: Row(
              children: [
                IconButton(
                  icon: Icon(
                    FontAwesomeIcons.unlockAlt,
                    color: Colors.lightGreen,
                    size: 22.0,
                  ),
                  onPressed: () => {},
                ),
                Text(
                  'Челгу. КБиПА'.toUpperCase(),
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 20.0),
                FlatButton(
                  padding: EdgeInsets.all(5.0),
                  textColor: Colors.white,
                  color: Colors.redAccent,
                  child: Text(
                    'Выйти',
                  ),
                  onPressed: () async {
                    await logoutUser(phone_number: this.user_phone);

                    Timer(Duration(milliseconds: 2000), () {
                      AppRouter()
                          .router()
                          .navigateTo(context, Constants.HOME_PAGE);
                    });
                  },
                ),
              ],
            ),
          )
        ],
      ),
      drawer: DrawerMenu(this.user.username, this.user.id),
      body: Container(
        alignment: Alignment.center,
        child: getPageWidgets(),
      ),
    );
  }

  Widget getPageWidgets() {
    if (this.authState == 0) {
      return Container(
        width: 700.0,
        child: Form(
          key: phoneFormKey,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                InternationalPhoneNumberInput(
                  onInputChanged: (PhoneNumber number) {
                    this.phone_number = number;
                  },
                  selectorConfig: SelectorConfig(
                    selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                  ),
                  ignoreBlank: false,
                  autoValidateMode: AutovalidateMode.disabled,
                  selectorTextStyle: TextStyle(color: Colors.black),
                  initialValue: phone_number,
                  hintText: 'Введите номер телефона',
                  errorMessage: 'Неправильный номер телефона',
                  textFieldController: phone_controller,
                  formatInput: false,
                  keyboardType: TextInputType.numberWithOptions(
                      signed: true, decimal: true),
                  inputBorder: OutlineInputBorder(),
                ),
                SizedBox(height: 30.0),
                Container(
                  margin: EdgeInsets.only(left: 130.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: 'Введите пароль',
                      border: OutlineInputBorder(
                          borderSide: BorderSide(width: 1, color: Colors.grey)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(width: 1, color: Colors.grey)),
                      errorBorder: InputBorder.none,
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(width: 1, color: Colors.grey)),
                      contentPadding: EdgeInsets.only(
                          left: 13, bottom: 11, top: 11, right: 15),
                    ),
                    key: this.codeFormKey,
                    controller: code_controller,
                    obscureText: true,
                  ),
                ),
                SizedBox(height: 30.0),
                MaterialButton(
                  elevation: 3.0,
                  minWidth: 280.0,
                  height: 65.0,
                  child: setUpButtonChild(),
                  onPressed: () {
                    print(phone_controller.text);

                    if (_buttonState == 0) {
                      setState(() {
                        _buttonState = 1;
                      });
                    }

                    _authVk();
                  },
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  splashColor: Colors.grey,
                ),
              ],
            ),
          ),
        ),
      );
    }
    // else if (this.authState == 1) {
    //   return Container(
    //     width: 700.0,
    //     child: Column(
    //       mainAxisAlignment: MainAxisAlignment.center,
    //       children: [
    //         Container(
    //           padding: const EdgeInsets.all(40.0),
    //           child: Column(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             children: <Widget>[

    //             ],
    //           ),
    //         ),
    //         MaterialButton(
    //           elevation: 3.0,
    //           minWidth: 230.0,
    //           height: 65.0,
    //           child: setUpButtonChild(),
    //           onPressed: () {
    //             if (_buttonState == 0) {
    //               setState(() {
    //                 _buttonState = 1;
    //               });

    //               sendCode();
    //             }
    //           },
    //           color: Colors.lightBlue,
    //           textColor: Colors.white,
    //           padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
    //           splashColor: Colors.grey,
    //         ),
    //       ],
    //     ),
    //   );
    //}
    else {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                children: <Widget>[
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
                    child: Center(
                      child: SafeArea(
                        child: Row(
                          children: [
                            _appState == ApplicationStatus.NONE ||
                                    _appState == ApplicationStatus.STOPPED ||
                                    _appState == ApplicationStatus.FINISH
                                ? Expanded(
                                    flex: 1,
                                    child: getToolsArea(),
                                  )
                                : Expanded(
                                    flex: 1,
                                    child: AbsorbPointer(
                                      child: getAnimationProcessingContainer(
                                        getToolsArea(),
                                      ),
                                    ),
                                  ),
                            Expanded(
                              flex: 3,
                              child: Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.all(20.0),
                                    padding: EdgeInsets.all(16.0),
                                    height: MediaQuery.of(context).size.height,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Color.fromRGBO(49, 87, 110, 1.0),
                                      ),
                                    ),
                                    child: SingleChildScrollView(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          channelListtextArea(),
                                          Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  FileUploadApp(),
                                                  getStartAndStopButton(),
                                                ],
                                              ),
                                              SizedBox(height: 10.0),
                                              startSearchImages(),
                                            ],
                                          ),
                                          // progresMessageTextArea(Constants.EMPTY_LOG_MESSAGE),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }
  }

  /*
    Метод для авторизации через телеграм на сервере
    @return - Значение bool
  */
  Future<bool> _authVk() async {
    String telegramAuthPhone = Constants.API_SERVER + Constants.VKONTAKTE_LOGIN;

    final response = await http.post(
      telegramAuthPhone,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, dynamic>{
          "phone": this.phone_number.phoneNumber,
          "password": this.code_controller.text,
        },
      ),
    );

    print(response.statusCode);
    if (response.statusCode == 200) {
      setState(() {
        this.authState = 1; // Авторизовались
      });

      final prefs = await SharedPreferences.getInstance();
      prefs.setString('phone_vk', phone_number.phoneNumber);

      print('Успешно авторизовались');
    } else {
      print(response.statusCode);
      setState(() {
        this.authState = 0; // Авторизовались
        this._buttonState = 0;
      });

      ShowErrorDialogMessage('Ошибка авторизации', 'Неправильный логин или пароль!', context);
    }

    return true;
  }

  Future<bool> sendCode() async {
    String telegramAuthCode = Constants.API_SERVER + Constants.TELEGRAM_LOGIN;

    final response = await http.post(
      telegramAuthCode,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, dynamic>{
          "code": this.code_controller.text,
        },
      ),
    );
  }

  Widget setUpButtonChild() {
    if (_buttonState == 0) {
      return Text("Войти через Вконтакте", style: TextStyle(fontSize: 22.0));
    } else if (_buttonState == 1) {
      return CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    } else {
      return Icon(Icons.check, color: Colors.white);
    }
  }

  Theme channelListtextArea() {
    return Theme(
      data: ThemeData(
        primaryColor: Colors.blue[800],
        primaryColorDark: Colors.blue[800],
      ),
      child: Container(
        margin: EdgeInsets.only(top: 5.0),
        child: TextField(
          controller: SitesPage.sourcesController,
          keyboardType: TextInputType.multiline,
          maxLines: 8,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.purple),
            ),
            hintText:
                'Укажите список идентификаторов источников Вконтакте. \nКаждый идентификаторов с новой строки!',
            helperText: 'Пример: -38006370',
            labelText: 'Список источников',
            labelStyle: TextStyle(
              fontSize: 16.0,
              color: Colors.black,
            ),
            prefixIcon: const Icon(
              Icons.list_alt,
              color: Colors.purple,
            ),
            prefixText: ' ',
            suffixText: 'ЧелГУ. КБиПА',
            suffixStyle: const TextStyle(color: Colors.redAccent),
          ),
        ),
      ),
    );
  }

  Widget getStartAndStopButton() {
    if (_appState == ApplicationStatus.NONE ||
        _appState == ApplicationStatus.FINISH ||
        _appState == ApplicationStatus.STOPPED) {
      return Container(
        child: MaterialButton(
          elevation: 5.0,
          minWidth: 200.0,
          height: 50,
          color: Colors.green,
          child: Text(
            'Начать поиск',
            style: TextStyle(
                fontSize: 16.0,
                color: Colors.white,
                fontWeight: FontWeight.bold),
          ),
          onPressed: () {
            setState(() {
              _appState = ApplicationStatus.START;
            });
          },
        ),
      );
    } else {
      return Container(
        margin: EdgeInsets.all(20.0),
        child: MaterialButton(
          elevation: 5.0,
          minWidth: 200.0,
          height: 50,
          color: Colors.redAccent,
          child: Text(
            'Остановить',
            style: TextStyle(
              fontSize: 16.0,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          onPressed: () {
            setState(() {
              _appState = ApplicationStatus.STOPPED;
            });
          },
        ),
      );
    }
  }

  Widget startSearchImages() {
    if (_appState == ApplicationStatus.START ||
        _appState == ApplicationStatus.PROCESSING) {
      if (_appState == ApplicationStatus.START) {
        // Начинаем поиск
        startSearching();
      }

      setState(() {
        _appState = ApplicationStatus.PROCESSING;
      });

      return Container(
        margin: EdgeInsets.all(10.0),
        child: StreamBuilder<Progress>(
          stream: Stream.periodic(Duration(seconds: 2))
              .takeWhile((_) => _progress != 100)
              .asyncMap((i) => getProgress()),
          initialData: Progress(),
          builder: (context, streams) {
            if (streams.hasData) {
              Progress result = streams.data;
              double progress = result.progress != null ? result.progress : 0;
              logMessage = result.logMessage != null ? result.logMessage : '';

              if (progress >= 100) {
                if (result.resultArhciveLink != null) {
                  html.window.open(
                      Constants.API_SERVER + result.resultArhciveLink,
                      '_blank');
                }

                SchedulerBinding.instance.addPostFrameCallback((_) async {
                  setState(() {
                    _appState = ApplicationStatus.FINISH;
                  });
                });
              }

              return Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(20.0),
                    child: LinearPercentIndicator(
                      animation: true,
                      animationDuration: 500,
                      alignment: MainAxisAlignment.center,
                      center: Text(
                        progress.toString() + '%',
                        style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      width: 400.0,
                      lineHeight: 35.0,
                      animateFromLastPercent: true,
                      percent: progress / 100,
                      backgroundColor: Colors.grey,
                      linearGradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Colors.deepPurpleAccent,
                          Colors.purple[800],
                        ],
                      ),
                    ),
                  ),
                  progresMessageTextArea(logMessage),
                ],
              );
            } else {
              SchedulerBinding.instance.addPostFrameCallback((_) {
                setState(() {
                  _appState = ApplicationStatus.NONE;
                });
                return ShowErrorDialogMessage(
                    "Ошибка",
                    "Не удалось получить данные от сервера.\nПожалуйста, попробуй позже!",
                    this.context);
              });

              return Container();
            }
          },
        ),
      );
    } else {
      return _appState != ApplicationStatus.FINISH
          ? progresMessageTextArea(Constants.EMPTY_LOG_MESSAGE)
          : progresMessageTextArea(logMessage);
    }
  }

  Column progresMessageTextArea(logMessage) {
    return Column(
      children: [
        Card(
          color: Color.fromRGBO(49, 87, 110, 1.0),
          elevation: 4.0,
          child: ListTile(
            leading: Icon(
              Icons.check,
              color: Colors.white,
            ),
            title: Text(
              'Лог работы'.toUpperCase(),
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
          ),
        ),
        Column(
          children: [
            Container(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Card(
                  elevation: 4.0,
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      height: 200,
                      margin: EdgeInsets.symmetric(
                        horizontal: 16.0,
                        vertical: 5.0,
                      ),
                      child: SingleChildScrollView(
                          scrollDirection: Axis.vertical, //.horizontal
                          child: Html(
                            data: logMessage,
                          )),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  _appState == ApplicationStatus.PROCESSING
                      ? RaisedButton(
                          hoverColor: Colors.purple[600],
                          padding: EdgeInsets.all(20.0),
                          textColor: Colors.white,
                          color: Colors.purple,
                          child: Text(
                            'Сохранить лог в файл',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          onPressed: () async {},
                        )
                      : SizedBox(),
                  SizedBox(width: 20.0),
                  RaisedButton(
                    hoverColor: Color.fromRGBO(49, 70, 100, 1.0),
                    padding: EdgeInsets.all(20.0),
                    textColor: Colors.white,
                    color: Color.fromRGBO(49, 87, 110, 1.0),
                    child: Text(
                      'Очистить лог',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    onPressed: () async {
                      if (!await clearServerLog() == true) {
                        ShowErrorDialogMessage(
                            'Ошибка',
                            'При очистке сообщений лог сервера возникла проблема.\nПожалуйста, попробуй позже!',
                            this.context);
                      }
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ],
    );
  }

  Future<bool> clearServerLog() async {
    String clearLog = Constants.API_SERVER + Constants.CLEAR_LOG;
    final response = await http.get(clearLog);

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Возникли непредвиденные ошибки!');
    }
  }

  Widget getToolsArea() {
    return Container(
      margin: EdgeInsets.all(16.0),
      padding: EdgeInsets.all(16.0),
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        border: Border.all(
          color: Color.fromRGBO(49, 87, 110, 1.0),
        ),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.settings),
              SizedBox(width: 5.0),
              Text('Настройки системы'.toUpperCase()),
            ],
          ),
          SizedBox(height: 20.0),
          getLimitParam(),
          SizedBox(height: 20.0),
          getAccuracyWidget(),
        ],
      ),
    );
  }

  Future<Progress> getProgress() async {
    String progressURl = Constants.API_SERVER + Constants.PROGRESS_ENDPOINT;
    final response = await http.get(progressURl);

    if (response.statusCode == 200) {
      return Progress.fromJson(json.decode(response.body));
    } else {
      throw Exception('Возникли непредвиденные ошибки!');
    }
  }

  void startSearching() async {
    if (_appState != ApplicationStatus.PROCESSING) {
      print(user_phone);
      print(SitesPage.sourcesController.text);
      print(user.id);
      print(_limitValue);
      print(_accuracyPercent);
      final finalResult = await http.post(
        Constants.API_SERVER + Constants.VKONTAKTE_SEARCH,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, dynamic>{
          'channels': SitesPage.sourcesController.text,
          'phone': user_phone,
          'user_id': user.id,
          'limit': _limitValue,
          'accuracy': _accuracyPercent,
        }),
      );
    }
  }

  /*
    Анимация верхнего слоя контейнера с параметрами
  */
  Widget getAnimationProcessingContainer(Widget toolsArea) {
    return Stack(
      children: [
        toolsArea,
        Container(
          color: Colors.grey[400].withOpacity(0.4),
          margin: EdgeInsets.all(16.0),
          padding: EdgeInsets.all(16.0),
          height: MediaQuery.of(context).size.height,
        )
      ],
    );
  }

  /*  
    Метод возвращает виджет точности
  */
  Widget getAccuracyWidget() {
    return Column(
      children: [
        Tooltip(
          padding: EdgeInsets.all(15.0),
          textStyle:
              TextStyle(fontStyle: FontStyle.italic, color: Colors.white),
          message:
              'Точность системы при поиске изображений.\nСохранять изображения при совпадении\nот выбранного количества процентов!',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Icons.info,
                color: Color.fromRGBO(49, 87, 110, 0.95),
              ),
              SizedBox(width: 5.0),
              Text(
                'Точность поиска'.toUpperCase(),
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        SizedBox(height: 15.0),
        Container(
          height: 45.0,
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.blueAccent),
          ),
          child: Center(
            child: DropdownButton(
              isExpanded: true,
              value: _accuracyPercent,
              items: [
                DropdownMenuItem(child: Text("Любое совпадение"), value: 0),
                DropdownMenuItem(child: Text("От 5%"), value: 5),
                DropdownMenuItem(child: Text("От 15%"), value: 15),
                DropdownMenuItem(child: Text("От 30%"), value: 30),
                DropdownMenuItem(child: Text("От 50%"), value: 50),
                DropdownMenuItem(child: Text("От 75%"), value: 75),
              ],
              onChanged: (value) {
                setState(() {
                  _accuracyPercent = value;
                });
              },
            ),
          ),
        ),
      ],
    );
  }

  /*
    Метод возвращает виджет параметра лимит
  */
  Widget getLimitParam() {
    return Column(
      children: [
        Tooltip(
          padding: EdgeInsets.all(15.0),
          textStyle:
              TextStyle(fontStyle: FontStyle.italic, color: Colors.white),
          message:
              'Параметр, указывающий максимальное количество записей на канале,\nкоторые необходимо исследовать',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Icons.info,
                color: Color.fromRGBO(49, 87, 110, 0.95),
              ),
              SizedBox(width: 5.0),
              Text(
                'Количество записей'.toUpperCase(),
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        SizedBox(height: 15.0),
        Slider(
          value: _limitValue,
          min: 1,
          max: 1000,
          divisions: 200,
          label: _limitValue.round().toString(),
          onChanged: (double value) {
            setState(() {
              _limitValue = value;
            });
          },
        ),
      ],
    );
  }
}
