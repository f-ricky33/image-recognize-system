import 'package:flutter/material.dart';
import 'package:pentest_web_app/routes/router.dart';
import 'package:pentest_web_app/utils/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Center(
        child: SizedBox(
          width: 400,
          child: Card(
            child: SignUpForm(),
          ),
        ),
      ),
    );
  }
}

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _usernameTextController = TextEditingController();
  final _passwordTextController = TextEditingController();
  final _retypePasswordTextController = TextEditingController();
  final _registerFormKey = GlobalKey<FormState>();

  double _formProgress = 0;

  void _updateFormProgress() {
    var progress = 0.0;
    var controllers = [
      _usernameTextController,
      _passwordTextController,
      _retypePasswordTextController
    ];

    for (var controller in controllers) {
      if (controller.value.text.isNotEmpty) {
        progress += 1 / controllers.length;
      }
    }

    setState(() {
      _formProgress = progress;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _registerFormKey,
      onChanged: _updateFormProgress,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AnimatedProgressIndicator(value: _formProgress),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              'Регистрация',
              style: TextStyle(fontSize: 32.0, color: Colors.grey[600]),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _usernameTextController,
              decoration: InputDecoration(hintText: 'Логин'),
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Вы не заполнили логин!';
                }

                return null;
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _passwordTextController,
              decoration: InputDecoration(hintText: 'Пароль'),
              obscureText: true,
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Вы не заполнили пароль!';
                }

                if (value != _retypePasswordTextController.text) {
                  return 'Пароли не совпадают';
                }

                return null;
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _retypePasswordTextController,
              decoration: InputDecoration(hintText: 'Повторите пароль'),
              obscureText: true,
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Вы не заполнили пароль!';
                }

                if (value != _passwordTextController.text) {
                  return 'Пароли не совпадают';
                }

                return null;
              },
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 120.0,
                  height: 40.0,
                  child: TextButton(
                    style: ButtonStyle(
                      foregroundColor: MaterialStateColor.resolveWith(
                          (Set<MaterialState> states) {
                        return states.contains(MaterialState.disabled)
                            ? null
                            : Colors.white;
                      }),
                      backgroundColor: MaterialStateColor.resolveWith(
                          (Set<MaterialState> states) {
                        return states.contains(MaterialState.disabled)
                            ? null
                            : Colors.purple[800];
                      }),
                    ),
                    onPressed: () {
                      AppRouter()
                          .router()
                          .navigateTo(context, Constants.HOME_PAGE);
                    },
                    child: Text('Вход'),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 170.0,
                  height: 40.0,
                  child: TextButton(
                    style: ButtonStyle(
                      foregroundColor: MaterialStateColor.resolveWith(
                          (Set<MaterialState> states) {
                        return Colors.white;
                      }),
                      backgroundColor: MaterialStateColor.resolveWith(
                          (Set<MaterialState> states) {
                        return Colors.lightGreen;
                      }),
                    ),
                    onPressed: () async {
                      if (!_registerFormKey.currentState.validate()) {
                        return;
                      }

                      // Отправляем запрос на регистрацию
                      String login = _usernameTextController.text;
                      String pass = _passwordTextController.text;
                      await _registerUser(login, pass);
                    },
                    child: Text('Зарегистрироваться'),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  /*
    Метод для регистрации на сервере
    @login - логин пользователя
    @pass - пароль пользователя
    @return - возвращает обещание. Значение bool
  */
  Future<bool> _registerUser(login, pass) async {
    String registerPoint = Constants.API_SERVER + Constants.REGISTER;

    final response = await http.post(
      registerPoint,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, dynamic>{
          "username": login,
          "password": pass,
        },
      ),
    );

    if (response.statusCode == 200) {
      return true;
    } else if(response.statusCode == 409){
      print('Пользователь с таким логин уже существует!');
    } else {
      throw Exception('Возникли непредвиденные ошибки!');
    }
  }
}

class AnimatedProgressIndicator extends StatefulWidget {
  final double value;

  AnimatedProgressIndicator({
    @required this.value,
  });

  @override
  State<StatefulWidget> createState() {
    return _AnimatedProgressIndicatorState();
  }
}

class _AnimatedProgressIndicatorState extends State<AnimatedProgressIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Color> _colorAnimation;
  Animation<double> _curveAnimation;

  void initState() {
    super.initState();
    _controller = AnimationController(
        duration: Duration(milliseconds: 1200), vsync: this);

    var colorTween = TweenSequence([
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.red, end: Colors.orange),
        weight: 1,
      ),
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.orange, end: Colors.yellow),
        weight: 1,
      ),
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.yellow, end: Colors.green),
        weight: 1,
      ),
    ]);

    _colorAnimation = _controller.drive(colorTween);
    _curveAnimation = _controller.drive(CurveTween(curve: Curves.easeIn));
  }

  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    _controller.animateTo(widget.value);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) => LinearProgressIndicator(
        value: _curveAnimation.value,
        valueColor: _colorAnimation,
        backgroundColor: _colorAnimation.value.withOpacity(0.4),
      ),
    );
  }
}
