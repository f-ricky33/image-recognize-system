import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pentest_web_app/models/profile_model.dart';
import 'package:pentest_web_app/models/user_model.dart';
import 'package:pentest_web_app/routes/router.dart';
import 'package:pentest_web_app/utils/constants.dart';
import 'package:pentest_web_app/widgets/drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:html' as html;
import '../function.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  User user;
  int value = 0;

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => {_getUser().then((value) => this.user = value)});
  }

  Future<User> _getUser() async {
    final prefs = await SharedPreferences.getInstance();
    return User.fromJson(json.decode(prefs.getString('user')));
  }

  // FUTUREBUILDER

  @override
  Widget build(BuildContext context) {
    if (this.user == null) {
      Timer(Duration(milliseconds: 2000), () {
        setState(() {
          this.value = 1;
        });
      });

      return Scaffold(body: Center(child: CircularProgressIndicator()));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Система поиска изображений".toUpperCase(),
          style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
        ),
        elevation: .1,
        backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 50.0),
            child: Row(
              children: [
                IconButton(
                  icon: Icon(
                    FontAwesomeIcons.unlockAlt,
                    color: Colors.lightGreen,
                    size: 22.0,
                  ),
                  onPressed: () => {},
                ),
                Text(
                  'Челгу. КБиПА'.toUpperCase(),
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 20.0),
                FlatButton(
                  padding: EdgeInsets.all(5.0),
                  textColor: Colors.white,
                  color: Colors.redAccent,
                  child: Text(
                    'Выйти',
                  ),
                  onPressed: () async {
                    await logoutUser();

                    Timer(Duration(milliseconds: 2000), () {
                      AppRouter()
                          .router()
                          .navigateTo(context, Constants.HOME_PAGE);
                    });
                  },
                ),
              ],
            ),
          )
        ],
      ),
      drawer: DrawerMenu(this.user.username, this.user.id),
      body: FutureBuilder<List<Report>>(
        future: _getLinks(),
        builder: (BuildContext context, AsyncSnapshot<List<Report>> snapshot) {
          print(snapshot.data);
          if (snapshot.hasData) {
            return Container(
              margin: EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 5.0, top: 20.0),
                    child: Text(
                      "Добро пожаловать, " + this.user.username,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20.0, color: Colors.indigo),
                    ),
                  ),
                  Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
                    child: Table(
                      border: TableBorder.all(color: Colors.indigo),
                      children: [
                        TableRow(children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  FontAwesomeIcons.paperclip,
                                  color: Colors.indigoAccent,
                                  size: 20.0,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  "Название",
                                  textScaleFactor: 1.5,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  FontAwesomeIcons.link,
                                  color: Colors.indigoAccent,
                                  size: 20.0,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  "Ссылка на архив",
                                  textScaleFactor: 1.5,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ]),
                        for (var item in snapshot.data)
                          TableRow(children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                item.linkname,
                                textScaleFactor: 1.5,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: FlatButton(
                                    child: Text(
                                "Загрузить",
                                textScaleFactor: 1.5,
                                textAlign: TextAlign.center,
                              ),
                                    onPressed: () {
                                      html.window
                                          .open(Constants.API_SERVER + item.link, '_blank');
                                    }),
                            ),
                          ]),
                      ],
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Future<List<Report>> _getLinks() async {
    String loginPoint = Constants.API_SERVER + Constants.PROFILE;
    String params =
        '/?user_id=' + this.user.id.toString() + '&token=' + this.user.token;

    final response = await http.get(
      loginPoint + params,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      List<Report> reportList = [];

      for (Map i in data) {
        reportList.add(Report.fromJson(i));
      }

      return reportList;
    }
  }
}
