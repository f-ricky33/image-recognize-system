import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pentest_web_app/models/user_model.dart';
import 'package:pentest_web_app/routes/router.dart';
import 'package:pentest_web_app/utils/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../function.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Center(
        child: SizedBox(
          width: 400,
          child: Card(
            child: LoginForm(),
          ),
        ),
      ),
    );
  }
}

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Welcome!', style: Theme.of(context).textTheme.headline2),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<LoginForm> {
  final _usernameTextController = TextEditingController();
  final _passwordTextController = TextEditingController();
  final _loginFormKey = GlobalKey<FormState>();
  int _state = 0;

  double _formProgress = 0;

  /*
    Метод для обновления прогресса в форме
  */
  void _updateFormProgress() {
    var progress = 0.0;
    var controllers = [_passwordTextController, _usernameTextController];

    for (var controller in controllers) {
      if (controller.value.text.isNotEmpty) {
        progress += 1 / controllers.length;
      }
    }

    setState(() {
      _formProgress = progress;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _loginFormKey,
      onChanged: _updateFormProgress,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AnimatedProgressIndicator(value: _formProgress),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              'Вход в систему',
              style: TextStyle(fontSize: 32.0, color: Colors.grey[600]),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15.0),
            child: TextFormField(
              controller: _usernameTextController,
              decoration: InputDecoration(hintText: 'Логин'),
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Вы не заполнили логин!';
                }

                return null;
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15.0),
            child: TextFormField(
              controller: _passwordTextController,
              decoration: InputDecoration(hintText: 'Пароль'),
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Вы не заполнили пароль!';
                }

                return null;
              },
              obscureText: true,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 90.0,
                  child: MaterialButton(
                    child: setUpButtonChild(),
                    onPressed: () async {
                      if (!_loginFormKey.currentState.validate()) {
                        setState(() {
                          _state = 0;
                        });
                        return;
                      }

                      if (_state == 0) {
                        setState(() {
                          _state = 1;
                        });

                        String login = _usernameTextController.text;
                        String pass = _passwordTextController.text;

                        bool res = await _loginUser(login, pass);

                        if (res) {
                          setState(() {
                            _state = 2;
                          });
                          Timer(Duration(milliseconds: 2000), () {
                            AppRouter()
                                .router()
                                .navigateTo(context, Constants.DASHBOARD_PAGE);
                          });
                        } else {
                          setState(() {
                            _state = 0;
                          });
                        }
                      }
                    },
                    elevation: 4.0,
                    minWidth: double.infinity,
                    height: 48.0,
                    color: Colors.lightGreen,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 120.0,
                  height: 40.0,
                  child: TextButton(
                    style: ButtonStyle(
                      foregroundColor: MaterialStateColor.resolveWith(
                          (Set<MaterialState> states) {
                        return Colors.white;
                      }),
                      backgroundColor: MaterialStateColor.resolveWith(
                          (Set<MaterialState> states) {
                        return Colors.purple[800];
                      }),
                    ),
                    onPressed: () {
                      AppRouter()
                          .router()
                          .navigateTo(context, Constants.REGISTER_PAGE);
                    },
                    child: Text('Регистрация'),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget setUpButtonChild() {
    if (_state == 0) {
      return new Text(
        "Войти",
        style: const TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ),
      );
    } else if (_state == 1) {
      return CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    } else {
      return Icon(Icons.check, color: Colors.white);
    }
  }

  /*
    Мето для автоизации на сервере
    @login - логин существующего пользователя
    @pass - пароль существующего пользователя
    @return - возвращает обещание. Значение bool
  */
  Future<bool> _loginUser(login, pass) async {
    String loginPoint = Constants.API_SERVER + Constants.LOGIN;

    final response = await http.post(
      loginPoint,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, dynamic>{
          "username": login,
          "password": pass,
        },
      ),
    );

    if (response.statusCode == 200) {
      print(response.body);
      User user = User.fromJson(json.decode(response.body));

      final prefs = await SharedPreferences.getInstance();
      prefs.setString('user', json.encode(user));

      return true;
    } else if (response.statusCode == 400 || response.statusCode == 401) {
      ShowErrorDialogMessage(
          'Ошибка', 'Неправильный логин или пароль!', context);

      return false;
    } else {
      return false;
    }
  }
}

// --------------------------------- ВИДЖЕТЫ ДЛЯ АНИМАЦИИ ПРОГРЕССА В ФОРМЕ ------------------------------------------------

class AnimatedProgressIndicator extends StatefulWidget {
  final double value;

  AnimatedProgressIndicator({
    @required this.value,
  });

  @override
  State<StatefulWidget> createState() {
    return _AnimatedProgressIndicatorState();
  }
}

class _AnimatedProgressIndicatorState extends State<AnimatedProgressIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Color> _colorAnimation;
  Animation<double> _curveAnimation;

  void initState() {
    super.initState();
    _controller = AnimationController(
        duration: Duration(milliseconds: 1200), vsync: this);

    var colorTween = TweenSequence([
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.red, end: Colors.orange),
        weight: 1,
      ),
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.orange, end: Colors.yellow),
        weight: 1,
      ),
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.yellow, end: Colors.green),
        weight: 1,
      ),
    ]);

    _colorAnimation = _controller.drive(colorTween);
    _curveAnimation = _controller.drive(CurveTween(curve: Curves.easeIn));
  }

  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    _controller.animateTo(widget.value);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) => LinearProgressIndicator(
        value: _curveAnimation.value,
        valueColor: _colorAnimation,
        backgroundColor: _colorAnimation.value.withOpacity(0.4),
      ),
    );
  }
}
