// StreamBuilder<Progress>(
//     stream: sendRequests(),
//     initialData: Progress(),
//     builder: (context, streams) {
//       if (streams.hasData) {
//         print(streams.data
//             .toString());
//       } else {
//         return CircularProgressIndicator();
//       }
//     });
// ShowDialogMessage(
//     this._sourcesController.text,
//     context);
//  Response response = await this.sendRequest(_sourcesController.text);

//  //Создать класс для распарсивания json и распарсить по полям ответ
//  Progress progressModel= Progress.fromJson(json.decode(response.body));
//  print(progressModel.progress);

// class DashBoardPage extends StatefulWidget {
//   @override
//   _DashBoardPageState createState() => _DashBoardPageState();
// }

// class _DashBoardPageState extends State<DashBoardPage> {
//   Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
//   Future<String> _userParams;

//   @override
//   void initState() {
//     super.initState();
//     _userParams = _prefs.then((SharedPreferences prefs) {
//       return (prefs.getString('userparam') ?? null);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Text('123');

// SafeArea(
//   child: Scaffold(
//     body: FutureBuilder<String>(
//       future: _userParams,
//       builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
//         switch (snapshot.connectionState) {
//           case ConnectionState.waiting:
//             return const CircularProgressIndicator();
//           default:
//             if (snapshot.hasError || snapshot.data == null) {
//               Future.delayed(
//                 Duration(microseconds: 2000),
//                 () => {
//                   AppRouter()
//                       .router()
//                       .navigateTo(context, Constants.HOME_PAGE)
//                 },
//               );

//               return null;
//             } else {
//               final String userToken = snapshot.data.split('=')[1];
//               return DashBoardWidget(userToken: userToken);
//             }
//         }
//       },
//     ),
//   ),
// );
//   }
// }

// class DashBoardWidget extends StatelessWidget {
//   DashBoardWidget({this.userToken});

//   final String userToken;
//   DocRequest docsJson;

//   void getDocumentsByQuery(String query) async {
//     var response = await http.get(
//         'https://api.vk.com/method/docs.search?q=123&access_token=$userToken&v=5.21');

//     docsJson = DocRequest.fromJson(json.decode(response.body));
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: SafeArea(
//         child: Scaffold(
//           body: Container(
//               child: RaisedButton(
//             child: Text('get'),
//             onPressed: () {
//               getDocumentsByQuery('123');
//             },
//           )),
//         ),
//       ),
//     );
//   }
// }

import 'dart:convert';
import 'dart:html' as html;
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pentest_web_app/models/progress_model.dart';
import 'package:pentest_web_app/models/user_model.dart';
import 'package:pentest_web_app/routes/router.dart';
import 'package:pentest_web_app/widgets/fileupload.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:pentest_web_app/widgets/drawer.dart';
import 'package:flutter/material.dart';
import 'package:pentest_web_app/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../function.dart';

String logMessage = Constants.EMPTY_LOG_MESSAGE;

class SitesPage extends StatefulWidget {
  static TextEditingController sourcesController = TextEditingController();
  static TextEditingController get getTextAreaField => sourcesController;

  @override
  _SitesPageState createState() => _SitesPageState();
}

class _SitesPageState extends State<SitesPage> {
  int _appState = ApplicationStatus.NONE;

  int _useRequestHeader = 0;
  int _useDelayRange = 0;
  int _accuracyPercent = 0;
  int _progress = 0;
  bool _validateTextFieldEmpty = false;

  RangeValues _currentRangeDelayValues = const RangeValues(500, 1300);
  double _depthValue = 1;

  User user;

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => {_getUser().then((value) => this.user = value)});
  }

  Future<User> _getUser() async {
    final prefs = await SharedPreferences.getInstance();
    return User.fromJson(json.decode(prefs.getString('user')));
  }

  @override
  Widget build(BuildContext context) {
    if (this.user == null) {
      Timer(Duration(milliseconds: 2000), () {
        setState(() {
          this._depthValue = 1;
        });
      });

      return Scaffold(body: Center(child: CircularProgressIndicator()));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Система поиска изображений".toUpperCase(),
          style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
        ),
        elevation: .1,
        backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 50.0),
            child: Row(
              children: [
                IconButton(
                  icon: Icon(
                    FontAwesomeIcons.unlockAlt,
                    color: Colors.lightGreen,
                    size: 22.0,
                  ),
                  onPressed: () => {},
                ),
                Text(
                  'Челгу. КБиПА'.toUpperCase(),
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 20.0),
                FlatButton(
                  padding: EdgeInsets.all(5.0),
                  textColor: Colors.white,
                  color: Colors.redAccent,
                  child: Text(
                    'Выйти',
                  ),
                  onPressed: () async {
                    await logoutUser();

                    Timer(Duration(milliseconds: 2000), () {
                      AppRouter()
                          .router()
                          .navigateTo(context, Constants.HOME_PAGE);
                    });
                  },
                ),
              ],
            ),
          )
        ],
      ),
      drawer: DrawerMenu(this.user.username, this.user.id),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                children: <Widget>[
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
                    child: Center(
                      child: SafeArea(
                        child: Row(
                          children: [
                            _appState == ApplicationStatus.NONE ||
                                    _appState == ApplicationStatus.STOPPED ||
                                    _appState == ApplicationStatus.FINISH
                                ? Expanded(
                                    flex: 1,
                                    child: getToolsArea(),
                                  )
                                : Expanded(
                                    flex: 1,
                                    child: AbsorbPointer(
                                      child: getAnimationProcessingContainer(
                                        getToolsArea(),
                                      ),
                                    ),
                                  ),
                            Expanded(
                              flex: 3,
                              child: Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.all(20.0),
                                    padding: EdgeInsets.all(16.0),
                                    height: MediaQuery.of(context).size.height,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Color.fromRGBO(49, 87, 110, 1.0),
                                      ),
                                    ),
                                    child: SingleChildScrollView(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          sourceListtextArea(),
                                          Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  FileUploadApp(),
                                                  getStartAndStopButton(),
                                                ],
                                              ),
                                              SizedBox(height: 10.0),
                                              startSearchImages(),
                                            ],
                                          ),
                                          // progresMessageTextArea(Constants.EMPTY_LOG_MESSAGE),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Theme sourceListtextArea() {
    return Theme(
      data: ThemeData(
        primaryColor: Colors.blue[800],
        primaryColorDark: Colors.blue[800],
      ),
      child: Container(
        margin: EdgeInsets.only(top: 5.0),
        child: TextField(
          controller: SitesPage.sourcesController,
          keyboardType: TextInputType.multiline,
          maxLines: 8,
          decoration: InputDecoration(
            errorText: _validateTextFieldEmpty
                ? 'Пожалуйста, заполните данное поле!'
                : null,
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.purple),
            ),
            hintText:
                'Укажите список источников. Каждый источник с новой строки!',
            helperText: 'Пример: https://google.com',
            labelText: 'Список источников',
            labelStyle: TextStyle(
              fontSize: 16.0,
              color: Colors.black,
            ),
            prefixIcon: const Icon(
              Icons.list_alt,
              color: Colors.purple,
            ),
            prefixText: ' ',
            suffixText: 'ЧелГУ. КБиПА',
            suffixStyle: const TextStyle(color: Colors.redAccent),
          ),
        ),
      ),
    );
  }

  Widget getToolsArea() {
    return Container(
      margin: EdgeInsets.all(16.0),
      padding: EdgeInsets.all(16.0),
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        border: Border.all(
          color: Color.fromRGBO(49, 87, 110, 1.0),
        ),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.settings),
              SizedBox(width: 5.0),
              Text('Настройки системы'.toUpperCase()),
            ],
          ),
          SizedBox(height: 20.0),
          getSearchMode(),
          SizedBox(height: 20.0),
          this.getDelayWidget(),
          SizedBox(height: 20.0),
          getVpnWidget(),
          SizedBox(height: 20.0),
          getAccuracyWidget(),
        ],
      ),
    );
  }

  Column progresMessageTextArea(logMessage) {
    return Column(
      children: [
        Card(
          color: Color.fromRGBO(49, 87, 110, 1.0),
          elevation: 4.0,
          child: ListTile(
            leading: Icon(
              Icons.check,
              color: Colors.white,
            ),
            title: Text(
              'Лог работы'.toUpperCase(),
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
          ),
        ),
        Column(
          children: [
            Container(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Card(
                  elevation: 4.0,
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      height: 200,
                      margin: EdgeInsets.symmetric(
                        horizontal: 16.0,
                        vertical: 5.0,
                      ),
                      child: SingleChildScrollView(
                          scrollDirection: Axis.vertical, //.horizontal
                          child: Html(
                            data: logMessage,
                          )),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  _appState == ApplicationStatus.PROCESSING
                      ? RaisedButton(
                          hoverColor: Colors.purple[600],
                          padding: EdgeInsets.all(20.0),
                          textColor: Colors.white,
                          color: Colors.purple,
                          child: Text(
                            'Сохранить лог в файл',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          onPressed: () async {},
                        )
                      : SizedBox(),
                  SizedBox(width: 20.0),
                  RaisedButton(
                    hoverColor: Color.fromRGBO(49, 70, 100, 1.0),
                    padding: EdgeInsets.all(20.0),
                    textColor: Colors.white,
                    color: Color.fromRGBO(49, 87, 110, 1.0),
                    child: Text(
                      'Очистить лог',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    onPressed: () async {
                      if (!await clearServerLog() == true) {
                        ShowErrorDialogMessage(
                            'Ошибка',
                            'При очистке сообщений лог сервера возникла проблема.\nПожалуйста, попробуй позже!',
                            this.context);
                      }
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ],
    );
  }

  void startSearching() async {
    if (_appState != ApplicationStatus.PROCESSING) {
      final finalResult = await http.post(
        Constants.API_SERVER,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, dynamic>{
          'sourceList': SitesPage.sourcesController.text,
          'depthLevel': _depthValue,
          'useDelayRange': _useDelayRange,
          'delayStart':
              _useDelayRange != 0 ? _currentRangeDelayValues.start : 0,
          'delayEnd': _useDelayRange != 0 ? _currentRangeDelayValues.end : 0,
          'header': _useRequestHeader,
          'accuracy': _accuracyPercent,
          'user_id': user.id
        }),
      );
    }
  }

  /*
    1. Лог и прогресс
    2. Архивация всех текущих папок сайтов
    3. 

  */

  Future<Progress> getProgress() async {
    String progressURl = Constants.API_SERVER + Constants.PROGRESS_ENDPOINT;
    final response = await http.get(progressURl);

    if (response.statusCode == 200) {
      return Progress.fromJson(json.decode(response.body));
    } else {
      throw Exception('Возникли непредвиденные ошибки!');
    }
  }

  Widget getStartAndStopButton() {
    if (_appState == ApplicationStatus.NONE ||
        _appState == ApplicationStatus.FINISH ||
        _appState == ApplicationStatus.STOPPED) {
      return Container(
        child: MaterialButton(
          elevation: 5.0,
          minWidth: 200.0,
          height: 50,
          color: Colors.green,
          child: Text(
            'Начать поиск',
            style: TextStyle(
                fontSize: 16.0,
                color: Colors.white,
                fontWeight: FontWeight.bold),
          ),
          onPressed: () {
            setState(() {
              if (SitesPage.sourcesController.text.isEmpty) {
                _validateTextFieldEmpty = true;
                return;
              }
              _validateTextFieldEmpty = false;
              _appState = ApplicationStatus.START;
            });
          },
        ),
      );
    } else {
      return Container(
        margin: EdgeInsets.all(20.0),
        child: MaterialButton(
          elevation: 5.0,
          minWidth: 200.0,
          height: 50,
          color: Colors.redAccent,
          child: Text(
            'Остановить',
            style: TextStyle(
              fontSize: 16.0,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          onPressed: () {
            setState(() {
              _appState = ApplicationStatus.STOPPED;
            });
          },
        ),
      );
    }
  }

  Widget startSearchImages() {
    if (_appState == ApplicationStatus.START ||
        _appState == ApplicationStatus.PROCESSING) {
      if (_appState == ApplicationStatus.START) {
        // Начинаем поиск
        startSearching();
      }

      setState(() {
        _appState = ApplicationStatus.PROCESSING;
      });

      return Container(
        margin: EdgeInsets.all(10.0),
        child: StreamBuilder<Progress>(
          stream: Stream.periodic(Duration(seconds: 2))
              .takeWhile((_) => _progress != 100)
              .asyncMap((i) => getProgress()),
          initialData: Progress(),
          builder: (context, streams) {
            if (streams.hasData) {
              Progress result = streams.data;
              double progress = result.progress != null ? result.progress : 0;
              logMessage = result.logMessage != null ? result.logMessage : '';

              if (progress >= 100) {
                if (result.resultArhciveLink != null) {
                  html.window.open(
                      Constants.API_SERVER + result.resultArhciveLink,
                      '_blank');
                }

                SchedulerBinding.instance.addPostFrameCallback((_) async {
                  setState(() {
                    _appState = ApplicationStatus.FINISH;
                  });
                });
              }

              return Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(20.0),
                    child: LinearPercentIndicator(
                      animation: true,
                      animationDuration: 500,
                      alignment: MainAxisAlignment.center,
                      center: Text(
                        progress.toString() + '%',
                        style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      width: 400.0,
                      lineHeight: 35.0,
                      animateFromLastPercent: true,
                      percent: progress / 100,
                      backgroundColor: Colors.grey,
                      linearGradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Colors.deepPurpleAccent,
                          Colors.purple[800],
                        ],
                      ),
                    ),
                  ),
                  progresMessageTextArea(logMessage),
                ],
              );
            } else {
              SchedulerBinding.instance.addPostFrameCallback((_) {
                setState(() {
                  _appState = ApplicationStatus.NONE;
                });
                return ShowErrorDialogMessage(
                    "Ошибка",
                    "Не удалось получить данные от сервера.\nПожалуйста, попробуй позже!",
                    this.context);
              });

              return Container();
            }
          },
        ),
      );
    } else {
      return _appState != ApplicationStatus.FINISH
          ? progresMessageTextArea(Constants.EMPTY_LOG_MESSAGE)
          : progresMessageTextArea(logMessage);
    }
  }

  Future<bool> clearServerLog() async {
    String clearLog = Constants.API_SERVER + Constants.CLEAR_LOG;
    final response = await http.get(clearLog);

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Возникли непредвиденные ошибки!');
    }
  }

  Widget getDelayWidget() {
    return Column(children: [
      Tooltip(
        padding: EdgeInsets.all(15.0),
        textStyle: TextStyle(fontStyle: FontStyle.italic, color: Colors.white),
        message:
            'Задержка между запросами к сайту в секундах.\nДля избежания блокировки запросов\nрекомендуемся установить задержку между запросами!',
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.info,
              color: Color.fromRGBO(49, 87, 110, 0.95),
            ),
            SizedBox(width: 5.0),
            Text(
              'Использовать задержку?'.toUpperCase(),
              style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
      SizedBox(height: 15.0),
      Container(
        height: 45.0,
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.blueAccent),
        ),
        child: Center(
          child: DropdownButton(
            isExpanded: true,
            value: _useDelayRange,
            items: [
              DropdownMenuItem(child: Text("Нет"), value: 0),
              DropdownMenuItem(child: Text("Да"), value: 1),
            ],
            onChanged: (value) {
              setState(() {
                _useDelayRange = value;
              });
            },
          ),
        ),
      ),
      SizedBox(height: 15.0),
      _useDelayRange != 0
          ? RangeSlider(
              values: _currentRangeDelayValues,
              min: 0,
              max: 2000,
              divisions: 100,
              labels: RangeLabels(
                _currentRangeDelayValues.start.round().toString() + ' мс',
                _currentRangeDelayValues.end.round().toString() + ' мс',
              ),
              onChanged: (RangeValues values) {
                setState(() {
                  _currentRangeDelayValues = values;
                  print(_currentRangeDelayValues.start);
                });
              },
            )
          : Container(),
    ]);
  }

  /*
    Метод возвращает виджет VPN
  */
  Widget getVpnWidget() {
    return Column(
      children: [
        Tooltip(
          padding: EdgeInsets.all(15.0),
          textStyle:
              TextStyle(fontStyle: FontStyle.italic, color: Colors.white),
          message:
              'Позволяет задать собственные заголовки для запроса к источнику.\nПодробнее в разделе помощь!',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Icons.info,
                color: Color.fromRGBO(49, 87, 110, 0.95),
              ),
              SizedBox(width: 5.0),
              Text(
                'Установить заголовки?'.toUpperCase(),
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        SizedBox(height: 15.0),
        Container(
          height: 45.0,
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.blueAccent),
          ),
          child: Center(
            child: DropdownButton(
              isExpanded: true,
              value: _useRequestHeader,
              items: [
                DropdownMenuItem(child: Text("Нет"), value: 0),
                DropdownMenuItem(child: Text("Да"), value: 1),
              ],
              onChanged: (value) {
                setState(() {
                  _useRequestHeader = value;
                });
              },
            ),
          ),
        ),
      ],
    );
  }

  /*  
    Метод возвращает виджет точности
  */
  Widget getAccuracyWidget() {
    return Column(
      children: [
        Tooltip(
          padding: EdgeInsets.all(15.0),
          textStyle:
              TextStyle(fontStyle: FontStyle.italic, color: Colors.white),
          message:
              'Точность системы при поиске изображений.\nСохранять изображения при совпадении\nот выбранного количества процентов!',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Icons.info,
                color: Color.fromRGBO(49, 87, 110, 0.95),
              ),
              SizedBox(width: 5.0),
              Text(
                'Точность поиска'.toUpperCase(),
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        SizedBox(height: 15.0),
        Container(
          height: 45.0,
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.blueAccent),
          ),
          child: Center(
            child: DropdownButton(
              isExpanded: true,
              value: _accuracyPercent,
              items: [
                DropdownMenuItem(child: Text("Любое совпадение"), value: 0),
                DropdownMenuItem(child: Text("От 5%"), value: 5),
                DropdownMenuItem(child: Text("От 15%"), value: 15),
                DropdownMenuItem(child: Text("От 30%"), value: 30),
                DropdownMenuItem(child: Text("От 50%"), value: 50),
                DropdownMenuItem(child: Text("От 75%"), value: 75),
              ],
              onChanged: (value) {
                setState(() {
                  _accuracyPercent = value;
                });
              },
            ),
          ),
        ),
      ],
    );
  }

  /*
    Анимация верхнего слоя контейнера с параметрами
  */
  Widget getAnimationProcessingContainer(Widget toolsArea) {
    return Stack(
      children: [
        toolsArea,
        Container(
          color: Colors.grey[400].withOpacity(0.4),
          margin: EdgeInsets.all(16.0),
          padding: EdgeInsets.all(16.0),
          height: MediaQuery.of(context).size.height,
        )
      ],
    );
  }

  /*
    Метод возвращает виджет глубины поиска
  */
  Widget getSearchMode() {
    return Column(
      children: [
        Tooltip(
          padding: EdgeInsets.all(15.0),
          textStyle:
              TextStyle(fontStyle: FontStyle.italic, color: Colors.white),
          message:
              'Параметр, указывающий глубину просмотра страниц сайта.\nПодробнее в разделе "Помощь"',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Icons.info,
                color: Color.fromRGBO(49, 87, 110, 0.95),
              ),
              SizedBox(width: 5.0),
              Text(
                'Глубина поиска'.toUpperCase(),
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        SizedBox(height: 15.0),
        Slider(
          value: _depthValue,
          min: 1,
          max: 10,
          divisions: 9,
          label: _depthValue.round().toString(),
          onChanged: (double value) {
            setState(() {
              _depthValue = value;
            });
          },
        ),
      ],
    );
  }
}
