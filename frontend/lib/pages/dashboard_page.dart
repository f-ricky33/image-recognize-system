import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pentest_web_app/models/user_model.dart';
import 'package:pentest_web_app/routes/router.dart';
import 'package:pentest_web_app/utils/constants.dart';
import 'package:pentest_web_app/widgets/drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../function.dart';

class DashBoardPage extends StatefulWidget {
  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoardPage> {
  User user;
  int value = 0;

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => {_getUser().then((value) => this.user = value)});
  }

  Future<User> _getUser() async {
    final prefs = await SharedPreferences.getInstance();
    return User.fromJson(json.decode(prefs.getString('user')));
  }

  @override
  Widget build(BuildContext context) {
    if (this.user == null) {
      Timer(Duration(milliseconds: 2000), () {
        setState(() {
          this.value = 1;
        });
      });

      return Scaffold(body: Center(child: CircularProgressIndicator()));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Система поиска изображений".toUpperCase(),
          style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
        ),
        elevation: .1,
        backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 50.0),
            child: Row(
              children: [
                IconButton(
                  icon: Icon(
                    FontAwesomeIcons.unlockAlt,
                    color: Colors.lightGreen,
                    size: 22.0,
                  ),
                  onPressed: () => {},
                ),
                Text(
                  'Челгу. КБиПА'.toUpperCase(),
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 20.0),
                FlatButton(
                  padding: EdgeInsets.all(5.0),
                  textColor: Colors.white,
                  color: Colors.redAccent,
                  child: Text(
                    'Выйти',
                  ),
                  onPressed: () async {
                    await logoutUser();

                    Timer(Duration(milliseconds: 2000), () {
                      AppRouter()
                          .router()
                          .navigateTo(context, Constants.HOME_PAGE);
                    });
                  },
                ),
              ],
            ),
          )
        ],
      ),
      drawer: DrawerMenu(this.user.username, this.user.id),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            MaterialButton(
              elevation: 10.0,
              minWidth: 270.0,
              height: 120.0,
              child: Text("Веб-сайты", style: TextStyle(fontSize: 24.0)),
              onPressed: () {
                AppRouter()
                    .router()
                    .navigateTo(context, Constants.SITE_SOURCES_PAGE);
              },
              color: Colors.purple,
              textColor: Colors.white,
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              splashColor: Colors.grey,
            ),
            SizedBox(
              width: 30.0,
            ),
            MaterialButton(
              elevation: 10.0,
              minWidth: 270.0,
              height: 120.0,
              child: Text("Телеграм", style: TextStyle(fontSize: 24.0)),
              onPressed: () {
                AppRouter()
                    .router()
                    .navigateTo(context, Constants.TELEGRAM_PAGE);
              },
              color: Colors.blueAccent,
              textColor: Colors.white,
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              splashColor: Colors.grey,
            ),
             SizedBox(
              width: 30.0,
            ),
            MaterialButton(
              elevation: 10.0,
              minWidth: 270.0,
              height: 120.0,
              child: Text("Вконтакте", style: TextStyle(fontSize: 24.0)),
              onPressed: () {
                AppRouter()
                    .router()
                    .navigateTo(context, Constants.VKONTAKTE_PAGE);
              },
              color: Colors.green,
              textColor: Colors.white,
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              splashColor: Colors.grey,
            ),
          ],
        ),
      ),
    );
  }
}
