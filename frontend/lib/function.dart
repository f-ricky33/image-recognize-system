import 'package:flutter/material.dart';
import 'package:pentest_web_app/utils/constants.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

/*
  Функция для вывода alert-сообщений
  @title - заголовок окна
  @message - сообщение для вывода
  @context - контекст приложения
*/
void ShowErrorDialogMessage(
    String title, String message, BuildContext context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: title,
    desc: message,
    buttons: [
      DialogButton(
        child: Text(
          "Ок",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

/*
    Метод для отправки запроса деавтоизации на сервере
    @return - возвращает обещание. Значение bool
  */
Future<bool> logoutUser({phone_number}) async {
  String logoutUrl = Constants.API_SERVER + Constants.LOGOUT;

  logoutUrl += phone_number != null ? '/?phone=' + phone_number : '';
  final response = await http.get(logoutUrl);

  if (response.statusCode == 200) {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('user');

    if (phone_number != null) {
      final prefs = await SharedPreferences.getInstance();
      prefs.remove('phone');
      prefs.remove('phone_vk');
    }

    return true;
  } else {
    print(response.statusCode);
    throw Exception('Возникли непредвиденные ошибки!');
  }
}


// python3 -m http.server 8000